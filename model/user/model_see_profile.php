<?php

function getDBH(){
	require('../secret.php');
	$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
	return $dbh;
}

function getProfileName(){
	$result='';
	if(isset($_GET['email'])){
		try{
			$dbh=getDBH();
			$query=$dbh->prepare("select last_name,first_name from member where email=:email");
			$query->execute(array("email" => $_GET['email']));
			$fetch=$query->fetchALl();
			$result.=$fetch[0]['first_name'] . ' ' . $fetch[0]['last_name'];
		}
		catch(PDOException $e){
			echo $e->getMessage() . '<br> \n';
		}
	}
	return $result;
}

function getAmountEarned($email){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select money_earned from member where email=:email");
		$query->execute(array("email" => $email));
		$fetch=$query->fetchALl();
		$result.=$fetch[0]['money_earned'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
	return $result;
}

function getAmountSpent($email){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select money_spent from member where email=:email");
		$query->execute(array("email" => $email));
		$fetch=$query->fetchALl();
		$result.=$fetch[0]['money_spent'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
	return $result;
}

function getProfileContent(){
	$result='';
	if(isset($_GET['email'])){
		$name=getProfileName();
		$email=$_GET['email'];
		$money_earned=getAmountEarned($email);
		$money_spent=getAmountSpent($email);

		$result.='<h2>' . $name . '</h2>';
		$result.='<p>(' . $email . ')</p>';
		$result.='<p>Argent gagné : <b>' . $money_earned . '€</b></p>';
		$result.='<p>Argent dépensé : <b>' . $money_spent . '€</b></p>';

		$result.='<form method="post" action="send_warning_controller.php" id="send_warning">';
		$result.='<label for="reason">Envoyer un avertissement : <textarea name="reason" maxlength="250" form="send_warning" placeholder="Motif de l\'avertissement"></textarea></label><br>';
		$result.='<button type="submit" value="' . $email . '" name="email">Avertir</button></form>';

		$result.='<form method="post" action="suspend_controller.php">';
		$result.='<label for "suspend">Suspendre : <select name="duration">';
		$result.='<option value="1">1 jour</option>';
		$result.='<option value="3">3 jours</option>';
		$result.='<option value="7">7 jours</option>';
		$result.='<option value="-1">Indéfiniment</option>';
		$result.='</select></label><button type="submit" value="' . $email . '" name="email">Suspendre</button></form>';


	}
	return $result;
}
