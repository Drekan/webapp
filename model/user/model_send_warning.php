<?php


function sendWarning(){
	$result='';
	if(isset($_POST['email'])){
		try{
			require('../secret.php');
			$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
			$query=$dbh->prepare("insert into notification(creator,receiver,message,notif_type) values (:creator,:receiver,:message,'information')");
			$query->execute(array(
				"creator" => $_SESSION['email'],
				"receiver" => $_POST['email'],
				"message" => 'ADMIN : ' .$_POST['reason']
			));
			$result='L\'avertissement a bien été envoyé.';
		}
		catch(PDOException $e){
			echo $e->getMessage() . '<br> \n';
			$result='Erreur lors de la procédure.';
		}
	}

	return $result;
}
