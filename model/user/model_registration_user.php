<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	//Evite une répétition de code
/**
*\brief connexion automatique à la base de données
*\return database handler permettant d'accéder à la base de donnée
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}
	
/**
*\brief Gestion de l'inscription d'un nouveau member
*\return String, information sur le succès ou non le l'inscription
*
*
*
*/
function register()
	{
		$result = null;
		
		$email = null;
		$last_name = null;
		$first_name = null;
		$password = null;
		$password_confirmation = null;
		
		if(isset($_POST['email']))
		{		
			//on vérifie l'existence des différents élément de get afin d'éviter des erreurs lors de premier chargement de page
			$email = $_POST['email'];

			$dbh = dbConnect();

			//On vérifie que l'email n'est pas déjà dans la base de données
			$query = $dbh -> prepare("SELECT * from member where email=:email");

			$query -> execute(array(
				"email" => $email
			));
			
			$result_query = $query->fetch();

			if(isset($_POST['last_name']))
			{
				$last_name = $_POST['last_name'];
			}
			if(isset($_POST['first_name']))
			{
				$first_name = $_POST['first_name'];
			}
			if(isset($_POST['password']))
			{
				$password = md5($_POST['password']);
			}
			if(isset($_POST['password_confirmation']))
			{
				$password_confirmation = md5($_POST['password_confirmation']);
			}
			
			if(isset($result_query['email']))
			{
				$result = 'Cet email est déjà utilisé !<br/>';
			}
			//On vérifie que les deux mots de passe soient les mêmes, si oui, on crée le tuple concernant l'utilisateur dans la table users
			else if($password == $password_confirmation)
			{
				$register = $dbh -> prepare("INSERT INTO member(email,password,registration_date,last_name,first_name)
												VALUES(:email,:password,NOW(),:last_name,:first_name)");
				$register -> execute(array(
					"email" => $email,
					"password" => $password,
					"last_name" => $last_name,
					"first_name" => $first_name
				));
					
				//print_r($dbh->errorInfo());
					
				/*$verification = 'SELECT * FROM member;';
				foreach($dbh->query($verification) as $row) 
				{
					print($row['email']."\t".$row['password']."\t".$row['registration_date']."\t".$row['last_name']."\t".$row['first_name']."<br />");
				}*/
				$result = "Vous avez bien été inscrit";
			}
			//Sinon on affiche un message indiquant le problème
			else
			{
				$result = "Les deux mots de passe entrés sont différents";
			}
		}
		
		return $result;
	}		
?>
