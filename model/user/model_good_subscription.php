<?php 

/**
*\brief transforme un timestamp en chaîne de caractère
*\return String, date correspondant au timestamp
*
*
*
*/
function timestampToString($timestamp){
		return date('d / m / Y',$timestamp);
	}

/**
*\brief teste si un bien donné est disponible à une date donnée
*\return booléen, true si le bien est disponible, false sinon
*
*
*
*/
function isAvailable($timestamp,$id){
		$available=true;
		try{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);		
			$query = $dbh -> prepare("select * from borrow_good where good=:id");
			$query -> execute(array(
					"id" => $id
					));			
			if($query->rowCount()!=0){
				foreach($query as $row){ 
					if((int)(intval($row['day']) / 86400) == (int)($timestamp / 86400)){
						$available=false;
					}
				}
					}
		}
		catch(PDOException $e){
			echo $e->getMessage() . "<br/> \n";
		}

		return $available;
	}


/**
*\brief charge les 15 prochaines dates à laquelles un bien est disponible à l'emprunt
*\return String, conversion des dates en formulaire html
*
*
*
*/
function fillTimestampForm(){
		$result='';
		if(isset($_POST['good_id'])){
			$good_id=$_POST['good_id'];
			setcookie('good_id',$good_id);
			$current_timestamp=time();
			$date_processed=0;
			$day_offset=1;
			while($date_processed<15){
				$processing=$current_timestamp + (86400 * $day_offset);
				if(isAvailable($processing,$good_id)){
					$result.='<option value="' . $processing . '">' . timestampToString($processing) . '</option>';
					$date_processed++;
				}
				$day_offset++;
			}
		}
		else{
			$result='Erreur : pas de dates disponibles';
		}

		return $result;
	}





	
