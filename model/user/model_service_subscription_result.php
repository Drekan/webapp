<?php

/**
*\author SEGURA Bastien
*/

/**
*\brief connexion à la base de donnée
*\return si la connexion fonctionne ça retourne un database handle
*/
function getDBH(){
	require('../secret.php');
	$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
	return $dbh;
}

/**
*\brief récupère l'email du membre ayant proposé le service dont l'id est passé en paramètre
*\return l'email du membre
*/
function getServiceOwnerEmail($service_id){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select email_member from service where id=:id");
		$query->execute(array("id" => $service_id));
		$result=$query->fetchAll();
		$result=$result[0]['email_member'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief récupère le prix par heure correspondant au service dont l'id est passé en paramètre
*puis calcule le bon prix correspondant à la plage horaire dont l'id est également passé en paramètre
*\return le prix
*/
function getServicePrice($service_id,$timeslot_id){
	$result='';

	$start = 0;
	$end = 0;
	$hour = 0;
	
	try
	{
		$dbh = getDBH();

		$query = $dbh -> prepare("select price from service where id=:id");
		$query -> execute(array("id" => $service_id));

		$result = $query -> fetchAll();
		$result = $result[0]['price'];

		$getTime = $dbh -> prepare("select start_time, end_time from time_slot where id=:timeslot_id;");

		$getTime -> execute(array(
			"timeslot_id" => $timeslot_id
		));

		$getTime = $getTime -> fetch(PDO::FETCH_ASSOC);

		if(isset($getTime['start_time']))
		{
			$start = $getTime['start_time'];
		}
		if(isset($getTime['end_time']))
		{
			$end = $getTime['end_time'];
		}

		$hour = ($end - $start)/60;

		$result = $result * $hour;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief met à jour l'argent gagné par le membre ayant proposé le service
*ainsi que le nombre de biens et services qu'il a proposé et qui ont été empruntés par d'autre
*\return true si la modification a bien été effectuée, false sinon
*/
function updateOwnerJackpot($service_price,$owner_email){
	$result=false;
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("update member set money_earned=money_earned+:serviceprice where email=:email");
		$query->execute(array(
			"serviceprice" => $service_price,
			"email" => $owner_email
		));

		$query=$dbh->prepare("update member set bs_offered=bs_offered+1 where email=:email");
		$query->execute(array("email" => $owner_email));

		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief met à jour l'argent dépensé par le membre ayant emprunté le service
*ainsi que le nombre de biens et services qu'il a emprunté
*\return true si la modification a bien été effectuée, false sinon
*/
function updateApplicantJackpot($service_price){
	$result=false;
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("update member set money_spent=money_spent+:serviceprice where email=:email");
		$query->execute(array(
			"serviceprice" => $service_price,
			"email" => $_SESSION['email']
		));

		$query=$dbh->prepare("update member set bs_used=bs_used+1 where email=:email");
		$query->execute(array("email" => $_SESSION['email']));

		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief récupère le titre du service dont l'id est passé en paramètre
*\return le titre du service
*/
function getServiceTitle($service_id){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select title from service where id=:id");
		$query->execute(array("id" => $service_id));
		$fetch=$query->fetchAll();
		$result.=$fetch[0]['title'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n ';
	}

	return $result;
}

/**
*\brief récupère le nom et le prénom du membre empruntant le service
*\return le nom et le prénom du membre
*/
function getName($email){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select last_name,first_name from member where email=:email");
		$query->execute(array("email" => $email));
		$fetch=$query->fetchAll();
		$result.=$fetch[0]['first_name'] . ' ' . $fetch[0]['last_name'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n ';
	}

	return $result;
}

/**
*\brief crée la notification qui sera envoyée au membre ayant proposé le service dont l'id est passé en paramètre
*/
function updateNotification($ownerEmail,$service_id,$timestamp){
	$result=false;
	try{
		$message='Votre service intitulé \'' . getServiceTitle($service_id) . '\' a été sollicité pour le ' . date("d / m / Y",$timestamp) . ' par '
			. getName($_SESSION['email']);

		$dbh=getDBH();
		$query=$dbh->prepare("insert into notification(receiver,message,notif_type,date_borrow) values (:receiver,:message,'information',:date)");
		$query->execute(array(
			"receiver" => $ownerEmail,
			"message" => $message,
			"date" => $timestamp
		));
		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
}

/**
*\brief insère dans la base de données les données correspondant à l'emprunt du service
*dont l'id est récupéré grâce au table $_POST
*\return une phrase indiquant si l'insertion s'est bien passée ou non
*/
function getSubscriptionResult(){
	$result='';
	if(isset($_POST['timeslot']) AND isset($_COOKIE['service_id'])){
		$service_id=$_COOKIE['service_id'];
		$timeslot=explode(":",$_POST['timeslot']);
		$timeslot_id=$timeslot[0];
		$timestamp_service=$timeslot[1];


		try{
			$dbh=getDBH();
			$query=$dbh->prepare("INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (:service,:timeslot,:email,:day)");
			$query->execute(array(
				"service" => $service_id,
				"timeslot" => $timeslot_id,
				"email" => $_SESSION['email'],
				"day" => $timestamp_service
			));

			$result="Vous avez bien souscrit au service !";

			$ownerEmail=getServiceOwnerEmail($service_id);
			$servicePrice=getServicePrice($service_id,$timeslot_id);
			
			updateOwnerJackpot($servicePrice,$ownerEmail);
			updateApplicantJackpot($servicePrice);
			updateNotification($ownerEmail,$service_id,$timestamp_service);

		}
		catch(PDOException $e){
			echo $e->getMessage() . '<br>' ;
			$result="La souscription a échoué. Veuillez réessayer plus tard.";
		}
	}


	return $result;

}
