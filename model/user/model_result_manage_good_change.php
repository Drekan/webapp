<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief
*\return
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}


/**
*\brief
*\return
*
*
*
*/
function getChangeResult()
	{

		$result = '';
		$email = null;
		$title = null;
		$description = null;
		$image_link = null;
		$price = null;

		$id = null;
		$dbh = dbConnect();

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];
		}
			
		if(isset($_POST['title']) && !isset($_COOKIE['prevent_multiple_submit']))
		{
			setcookie('prevent_multiple_submit','true');

			//on récupère l'id correspondant au bien que l'on veut modifier
			if(isset($_POST['good_change_id']))
			{
				$id = $_POST['good_change_id'];
			}

			$title = $_POST['title'];
			$price = $_POST['price'];

			$update_title = $dbh -> prepare('UPDATE good
											 SET title=:title
											 WHERE email_member=:email
											 	AND id=:id;');

			$update_title -> execute(array(
				"title" => $title,
				"email" => $email,
				"id" => $id
			));

			$update_price = $dbh -> prepare('UPDATE good
											 SET price=:price
											 WHERE email_member=:email
											 	AND id=:id;');

			$update_price -> execute(array(
				"price" => $price,
				"email" => $email,
				"id" => $id
			));

			if(isset($_POST['description']))
			{
				$description = $_POST['description'];

				$update_description = $dbh -> prepare('UPDATE good
											 SET description=:description
											 WHERE email_member=:email
											 	AND id=:id;');

				$update_description -> execute(array(
					"description" => $description,
					"email" => $email,
					"id" => $id
				));
			}
			
			if(isset($_POST['image_link']))
			{
				$image_link = $_POST['image_link'];

				$update_image_link = $dbh -> prepare('UPDATE good
											 SET image_link=:image_link
											 WHERE email_member=:email
											 	AND id=:id;');

				$update_image_link -> execute(array(
					"image_link" => $image_link,
					"email" => $email,
					"id" => $id
				));
			}
					
			$result = "Votre modification de bien a bien été prise en compte !";
		}
		else
		{
			if(isset($_COOKIE['prevent_multiple_submit']))
			{
				$result = "Votre modification de bien a bien été prise en compte !";
			}
			else
			{
				$result = "Oh, bonjour vous ! On ne vous attendait pas ici :o";
			}
		}

		return $result;
	}
