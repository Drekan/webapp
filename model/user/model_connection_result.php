<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief fonction qui automatise la connexion à la database
*\return database holder
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief vérifie si le compte d'un utilisateur donné est suspendu ou non
*\return booléen, true si l'utilisateur est suspendu
*
*
*
*/
function isSuspended($email){
		$result=false;
			
		$dbh=dbConnect();

		if($dbh != null)
		{
			$query=$dbh->prepare("SELECT email from suspended where email=:email");
			$query->execute(array("email" => $email));
			$fetch=$query->fetchAll();
			if(isset($fetch[0])){
				$result=true;
			}
		}

		return $result;
	}

/**
*\brief récupère la durée de suspention d'un compte
*\return int, la durée de suspention si le compte est suspendu, -1 sinon
*
*
*
*/
function getDuration($email)
	{
		$duration = -1;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT suspention_duration FROM suspended WHERE email=:email;");

			$query -> execute(array(
				"email" => $email
			));

			$query = $query -> fetch(PDO::FETCH_ASSOC);

			if(isset($query['suspention_duration']))
			{
				$duration = $query['suspention_duration'];
			}
		}

		return $duration;
	}

/**
*\brief fonction qui permet de connaître la date de suspention d'un compte à partir d'un email
*\return int, la date de suspention d'un compte (en timestamp), -1 sinon
*
*
*
*/
function getSuspensionDate($email)
	{
		$suspensionDate = -1;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT suspention_date FROM suspended WHERE email=:email;");

			$query -> execute(array(
				"email" => $email
			));

			$query = $query -> fetch(PDO::FETCH_ASSOC);

			if(isset($query['suspention_date']))
			{
				$suspensionDate = $query['suspention_date'];
			}
		}

		return $suspensionDate;
	}

/**
*\brief Informe l'utilisateur dont le compte a été suspendu de la durée de la suspension
*\return String, message d'information
*
*
*
*/
function timeOfSuspension($email)
	{
		$result = null;

		//On récupère la durée de la suspension en secondes
		$timeOfSuspension = getDuration($email);

		//On calcule le nombre de jours
		$numberDay = (int)($timeOfSuspension/86400);

		//On récupère la date de suspension en secondes
		$suspensionDate = getSuspensionDate($email);

		$now = time();

		$remainingTime = (($suspensionDate + $timeOfSuspension) - $now);
		$remainingTime = (int)($remainingTime/86400);

		if($numberDay <= 7)
		{

			if($numberDay == 1)
			{
				$result .= "Votre compte a été suspendu pour une durée d'1 jour.";
			}
			else
			{
				$result .= "Votre compte a été suspendu pour une durée de ".$numberDay." jours. Il reste ".$remainingTime." jours de suspension";
			}
		}
		else
		{
			$result .= "Votre compte a été suspendu définitivement.";
		}

		return $result;
	}

/**
*\brief La fonction essaie de connecter l'utilisateur (charger une session) s'il existe
*\return String, message d'information sur l'état de la connexion
*
*
*
*/
function getConnectionResult(){
		$result=null;
		$email=null;
		$password=null;

		if(isset($_POST['email'])){
			$email=$_POST['email'];
		}

		if(isset($_POST['password'])){
			$password=md5($_POST['password']);
		}

		try{
			require('../secret.php');
			
			$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
			$query=$dbh->prepare("SELECT * from member where email=:email AND password=:password");

			$query->execute(array("email" => $email, "password" => $password));
			
			$result_query=$query->fetch();
		}
		catch(PDOException $e){
			echo $e->getMessage() . '<br/>';
			$result="Problème lors de la connexion";
		}
	
		if(isset($result_query['email'])){
			if($result_query==''){
				$result='L\'utilisateur n\'existe pas ou le mot de passe est incorret. <br/>';
			}else if(!isSuspended($result_query['email'])){
				$_SESSION['email']=$result_query['email'];
				$result='Bonjour, ' . $result_query['first_name'] . '<br/>';
			}
			else{
				$result=timeOfSuspension($email);
			}
		}else{
			$result='L\'utilisateur n\'existe pas ou le mot de passe est incorret. <br/>';
		}

		return $result;
	}

