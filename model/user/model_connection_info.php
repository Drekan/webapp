<?php

/**
*\brief fonction qui récupère les infos de connexions pour pouvoir les afficher
*\return les balises du header du site correspondantes
*
*
*
*/
function getInfoConnection(){
	if(!isset($_POST['timeslot']) AND isset($_COOKIE['service_id'])){
		setcookie('service_id','false',1);
	}

	if(!isset($_POST['day']) AND isset($_COOKIE['good_id'])){
		setcookie('good_id','false',1);
	}

	if(!isset($_POST['title']) && isset($_COOKIE['prevent_multiple_submit'])){
		setcookie('prevent_multiple_submit','false',1);
	}
	
	if(!isset($_POST['title']) && !isset($_POST['day'])){
		if(isset($_COOKIE['timeslot'])){
			setcookie('timeslot','suppression_des_timeslots',1);
		}
	}
	$return= null;
	if(isset($_SESSION['email'])){
		ob_start(); ?>
		<li><a href="submit_choice_controller.php">Déposer une annonce</a></li>	
		<li class="connection_info"><a href="logout_controller.php">Se déconnecter</a></li>
		<li class="connection_info"><a href="my_profile_controller.php">Mon profil</a></li>
		<li class="connection_info"><a href="notification_controller.php">Notifications</a></li>
	<?php	$return=ob_get_clean();
	}else {
		ob_start(); ?>
		<li class="connection_info"><a href="connection_controller.php">Se connecter</a></li>
		<li class="connection_info"><a href="registration_controller.php">S'inscrire</a></li>
	<?php 	$return=ob_get_clean();
	}
	return $return;
}

