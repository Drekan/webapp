<?php

/**
*\brief insère un bien dans la base de donnée
*\return String, information sur le succès ou non de l'insertion
*
*
*
*/
function getSubmitionResult(){

		$result='';
		$email=null;
		$title=null;
		$description=null;
		$image_link=null;
		$price=null;
			
		if(isset($_POST['title']) && !isset($_COOKIE['prevent_multiple_submit'])){
			setcookie('prevent_multiple_submit','true');
			$title=$_POST['title'];
			$price=$_POST['price'];

			if(isset($_POST['description'])){
				$description=$_POST['description'];	
			}
			
			if(isset($_POST['image_link'])){
				$header=@get_headers($_POST['image_link']);
				if($header AND $header[0] != 'HTTP/1.1 404 Not Found'){
					if(getimagesize($_POST['image_link'])){
						$image_link=$_POST['image_link'];
					}
					else{
						echo 'nop';
					}
				}
			}

			if(isset($_SESSION['email'])){
				$email=$_SESSION['email'];
			}
			if($price>0){
				try{
					require('../secret.php');
					$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

					$submit = $dbh -> prepare("INSERT INTO good(title,description,price,image_link,email_member) VALUES (:title,:description,:price,:image_link,:email)");
					$submit -> execute(array(
						"title" => $title,
						"description" => $description,
						"price" => (int)($price / 50)+1,
						"image_link" => $image_link,
						"email" => $email
					));

					$result = "Votre offre de bien a bien été posté !";
				}
				catch(PDOException $e){
					echo $e->getMessage()."<br/>\n";
					//die("Connexion impossible !");
					$result = "Problème lors de la soumission de l'offre";
				}
			}
			else{
				$result="Impossible de mettre un prix < 0 !";
			}
		}else{
			if(isset($_COOKIE['prevent_multiple_submit'])){
				$result="Votre offre de bien a bien été posté !";
			}else{
				$result="Oh, bonjour vous ! On ne vous attendait pas ici :o";
			}
		}
		return $result;
	}
