<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief charge les créneaux d'un service
*\return String, html correspondant aux créneaux du service
*
*
*
*/
function timeslotService()
	{
		$display = null;
		$email = null;
		$id_service = null;

		$start_hour = 0;
		$start_minute = 0;
		$end_hour = 0;
		$end_minute = 0;

		$id_is_offered = null;

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];

			try
			{
				require('../secret.php');
				$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

				if(isset($_POST['timeslot_manage_id']))
				{
					$id_service = $_POST['timeslot_manage_id'];
				}
				
				$query = $dbh -> prepare("SELECT time_slot.id as id_timeslot, service.id, start_time, end_time, day 
							FROM service,is_offered,time_slot
							WHERE email_member=:email
									AND service.id=:id
									AND service.id=service
									AND time_slot.id=time_slot;");

				$query -> execute(array(
					"email" => $email,
					"id" => $id_service
				));

				$display .= '<form method="post" action="timeslot_add_controller.php" id="timeslot_add_form" class="display_timeslot">';

				$display .= '<button type="submit" value="'.$id_service.'" name="timeslot_add_id">Ajouter un horaire</button>';

				$display .= '</form>';

				while($row = $query->fetch())
				{
					
					$start_minute = $row['start_time'] % 60;
					$start_hour = ($row['start_time'] - $start_minute) / 60;

					$end_minute = $row['end_time'] % 60;
					$end_hour = ($row['end_time'] - $end_minute) / 60;

					$display .= '<form method="post" action="manage_timeslot_controller.php" id="manage_timeslot_change_form" class="display_timeslot">';

					$display .= '<p>'.$row['day'].' de ';

					if($start_hour < 10)
					{
						$display .= "0".$start_hour."h";
					}
					else
					{
						$display .= $start_hour."h";
					}

					if($start_minute < 10)
					{
						$display .= "0".$start_minute." à ";
					}
					else
					{
						$display .= $start_minute." à ";
					}


					if($end_hour < 10)
					{
						$display .= "0".$end_hour."h";
					}
					else
					{
						$display .= $end_hour."h";
					}

					if($end_minute < 10)
					{
						$display .= "0".$end_minute;
					}
					else
					{
						$display .= $end_minute."</br>";
					}

					$display .= '</p>';

					$id_is_offered = $row['id_timeslot'].'/'.$id_service;

					$display .= '<button type="submit" value="'.$id_is_offered.'" name="timeslot_change_id">Modifier</button>';
					
					$display .= '<button type="submit" value="'.$id_is_offered.'" name="timeslot_suppression_id">Supprimer</button>';
					
					$display .= '</form>';
				}
			}
			catch(PDOException $e)
			{
				echo $e->getMessage()."<br/>\n";
				//die("Connexion impossible !");
			}
		}

		return $display;
	}
?>