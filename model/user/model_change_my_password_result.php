<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	//Evite une répétition de code
/**
*\brief fonction de connexion à la base de donnée
*\return retourne le database holder
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}
	
/**
*\brief fonction d'insertion d'un nouveau mot de passe
*\return String qui indique si la requête s'est bien passé ou pas
*
* 
*
*/
function changePassword()
	{
		$result = null;
		
		$email = null;
		$password = null;
		$password_confirmation = null;
		
		$email = $_SESSION['email'];
			
		
		if(isset($_POST['new_password']))
		{
			$password = md5($_POST['new_password']);
		}
		if(isset($_POST['new_password_confirmation']))
		{
			$password_confirmation = md5($_POST['new_password_confirmation']);
		}
			
		$dbh = dbConnect();
		
		if($password == $password_confirmation)
		{
			$register = $dbh -> prepare("UPDATE member 
										 SET password=:password
										 WHERE email=:email");
			$register -> execute(array(
				"email" => $email,
				"password" => $password
			));
					
			$result = "Le mot de passe a bien été modifié";
		}
		//Sinon on affiche un message indiquant le problème
		else
		{
			$result = "Les deux mots de passe entrés sont différents";
		}
		
		return $result;
	}		
?>
