<?php


	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief
*\return
*
*
*
*/
function getDatabaseHandle(){
		$database_handle=null;
		try{
			require('../secret.php');
			$database_handle=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}catch(PDOException $exception){
			echo $exception->getMessage() . '<br/>';
		}
		return $database_handle;
	}

	
	

	//return value: id of the time_slot if it exists in the database, -1 otherwise
/**
*\brief
*\return
*
*
*
*/
function getTimeslotID($start_time,$end_time){
		$id=-1;
		$dbh=getDatabaseHandle();
		if($dbh!=null){
			$query= $dbh -> prepare("SELECT id FROM time_slot WHERE start_time=:start_time AND end_time=:end_time");
			$query->execute(array(
				"start_time" => $start_time,
				"end_time" => $end_time
				));
			$query=$query->fetch(PDO::FETCH_ASSOC);
			if(isset($query['id'])){
				$id=$query['id'];
			}
		
		}

		return $id;
	}

	//return value: id of the service if it exists in the db, -1 otherwise
/**
*\brief
*\return
*
*
*
*/
function getServiceID($title,$description,$price,$email){
		$id=-1;
		$dbh=getDatabaseHandle();
		if($dbh!=null){
			$query= $dbh -> prepare("SELECT id FROM service WHERE title=:title AND description=:description AND price=:price AND email_member=:email ORDER BY id DESC");
			$query->execute(array(
				"title" => $title,
				"description" => $description,
				"price" => $price,
				"email" => $email
				));
			$query=$query->fetch(PDO::FETCH_ASSOC);
			if(isset($query['id'])){
				$id=$query['id'];
			}
		}
		return $id;
	}
	
	
/**
*\brief
*\return
*
*
*
*/
function getSubmitionResult(){
		$result='';
		if(isset($_COOKIE['title'])){
			$title=$_COOKIE['title'];
		}
		
		if(isset($_COOKIE['description'])){
			$description=$_COOKIE['description'];
		}else{
			$description='';
		}
		
		if(isset($_COOKIE['price'])){
			$price=$_COOKIE['price'];
		}
		
		if(isset($_COOKIE['timeslot'])){
			$timeslot_array=unserialize($_COOKIE['timeslot']);
		}

		$dbh=getDatabaseHandle();

		if($dbh!=null && isset($_SESSION['email']) && isset($_COOKIE['title'])){
			if($price>0){
				$query= $dbh -> prepare("INSERT INTO service(title,description,price,email_member) VALUES (:title,:description,:price,:email)");

				$query-> execute(array(
					"title" => $title,
					"description" => $description,
					"price" => $price,
					"email" => $_SESSION['email']
					));

				$numberof_timeslot=count($timeslot_array);
				for($i=0;$i<$numberof_timeslot;$i++){
					if($timeslot_array[$i][3]==true){
						if(getTimeslotID($timeslot_array[$i][1],$timeslot_array[$i][2])==-1){
							$query= $dbh -> prepare("INSERT INTO time_slot(start_time,end_time) VALUES (:start_time,:end_time)");

							$query -> execute(array(
								"start_time" => $timeslot_array[$i][1],
								"end_time" => $timeslot_array[$i][2]
							));

						}
					}
				}

				$service_ID=getServiceID($title,$description,$price,$_SESSION['email']);
				for($i=0;$i<$numberof_timeslot;$i++){
					if(getTimeslotID($timeslot_array[$i][1],$timeslot_array[$i][2])!=-1){
						$timeslot_ID=getTimeslotID($timeslot_array[$i][1],$timeslot_array[$i][2]);

						$query= $dbh -> prepare("INSERT INTO is_offered(time_slot,service,day) VALUES (:time_slot,:service,:day)");

						$query -> execute(array(
							"time_slot" => $timeslot_ID,
							"service" => $service_ID,
							"day" => $timeslot_array[$i][0]
						));

					}
				}

				$result="Votre offre de service a bien été mise en ligne.";

				setcookie('timeslot','',1);
				setcookie('price','',1);
				setcookie('description','',1);
				setcookie('title','',1);
			}
			else{
				$result="Impossible de mettre un prix < 0 ! ";
			}
		}
		else{
			if(isset($_COOKIE['title'])){
				$result="Votre offre de service a bien été mise en ligne.";
			}

			if(!isset($_SESSION['email'])){
				$result="Vous n'êtes pas connectés !";
			}
		}

		return $result;
	}


















