<?php


/**
*\brief connexion automatique à la base de données
*\return database holder permettant d'accéder à la base
*
*
*
*/
function getDBH(){
	require('../secret.php');
	$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
	return $dbh;
}

/**
*\brief charge l'email du possesseur d'un bien donnée
*\return String, email d'un utilisateur
*
*
*
*/
function getGoodOwnerEmail($good_id){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select email from member,good where email=email_member and id=:id");
		$query->execute(array("id" => $good_id));
		$result=$query->fetchAll();
		$result=$result[0]['email'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief Charge le prix d'un bien donnée (sélectionné par son id)
*\return String, prix du bien
*
*
*
*/
function getGoodPrice($good_id){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select price from good where id=:id");
		$query->execute(array("id" => $good_id));
		$result=$query->fetchAll();
		$result=$result[0]['price'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief Fonction qui met à jour les cagnottes des utilisateurs concernés lorsqu'un emprunt est effectué
*\return Booléen, true si la requête s'est bien passé, false sinon
*
*
*
*/
function updateOwnerJackpot($good_price,$owner_email){
	$result=false;
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("update member set money_earned=money_earned+:goodprice where email=:email");
		$query->execute(array(
			"goodprice" => $good_price,
			"email" => $owner_email
		));

		$query=$dbh->prepare("update member set bs_offered=bs_offered+1 where email=:email");
		$query->execute(array("email" => $owner_email));

		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief Ajoute la somme dépensée à la cagnotte de l'utilisateur
*\return Booléen, true si la requête s'est bien passé, false sinon
*
*
*
*/
function updateApplicantJackpot($good_price){
	$result=false;
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("update member set money_spent=money_spent+:goodprice where email=:email");
		$query->execute(array(
			"goodprice" => $good_price,
			"email" => $_SESSION['email']
		));

		$query=$dbh->prepare("update member set bs_used=bs_used+1 where email=:email");
		$query->execute(array("email" => $_SESSION['email']));

		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $result;
}

/**
*\brief charge le titre d'un bien donné par son id
*\return String, le titre d'un bien
*
*
*
*/
function getGoodTitle($good_id){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select title from good where id=:id");
		$query->execute(array("id" => $good_id));
		$fetch=$query->fetchAll();
		$result.=$fetch[0]['title'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n ';
	}

	return $result;
}

/**
*\brief charge le nom d'un utilisateur donné par son email
*\return String, nom d'un utilisateur
*
*
*
*/
function getName($email){
	$result='';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select last_name,first_name from member where email=:email");
		$query->execute(array("email" => $email));
		$fetch=$query->fetchAll();
		$result.=$fetch[0]['first_name'] . ' ' . $fetch[0]['last_name'];
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n ';
	}

	return $result;
}

/**
*\brief Génere les notifications nécessaire lorsqu'un bien est emprunté
*return Booléen, true si la requête s'est bien passé, false sinon
*
*
*
*/
function updateNotification($ownerEmail,$good_id,$timestamp){
	$result=false;
	try{
		$message='Votre bien intitulé \'' . getGoodTitle($good_id) . '\' a été réservé pour le ' . date("d / m / Y",$timestamp) . ' par '
			. getName($_SESSION['email']);

		$dbh=getDBH();
		$query=$dbh->prepare("insert into notification(receiver,message,notif_type,date_borrow) values (:receiver,:message,'information',:date)");
		$query->execute(array(
			"receiver" => $ownerEmail,
			"message" => $message,
			"date" => $timestamp
		));
		$result=true;
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
}

/**
*\brief Enregistre l'emprunt d'un bien dans la base de donnée
*\return String, information sur le déroulement de l'insertion
*
*
*
*/
function getSubscriptionResult(){
	$timestamp=null;
	$result='';
	if(isset($_POST['day']) AND isset($_COOKIE['good_id'])){
		$timestamp=$_POST['day'];
		$email=$_SESSION['email'];
		$good_id=$_COOKIE['good_id'];
		try{
			$dbh=getDBH();
			$query=$dbh->prepare("INSERT INTO borrow_good(good,email_member,day) VALUES (:good,:email,:day)");
			$query->execute(array(
				"good" => $good_id,
				"email" => $email,
				"day" => $timestamp
			));

			$result="Le bien a bien été reservé pour le " . date("d / m / Y",$timestamp);

			$ownerEmail=getGoodOwnerEmail($good_id);
			$goodPrice=getGoodPrice($good_id);

			updateOwnerJackpot($goodPrice,$ownerEmail);
			updateApplicantJackpot($goodPrice);
			updateNotification($ownerEmail,$good_id,$timestamp);

		}catch(PDOException $e){
			echo $e->getMessage() . "<br/> \n";
			$result="Problème lors de la réservation du bien.";
		}
	}
	else{$result="oupsi";}
		return $result;
}
