<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);
/**
*\author DE BAERE Estelle
*/

/**
*\brief connexion à la base de donnée
*\return si la connexion fonctionne ça retourne un database handle
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief récupère l'id d'une plage horaire dans la table time_slot correspondant aux paramètres passés à la fonction
*\return l'id s'il existe, -1 sinon
*/
function getTimeslotID($start_time, $end_time)
	{
		$id = -1;
		$dbh = dbConnect();
		
		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT id FROM time_slot WHERE start_time=:start_time AND end_time=:end_time");
			$query->execute(array(
				"start_time" => $start_time,
				"end_time" => $end_time
				));
			$query=$query->fetch(PDO::FETCH_ASSOC);
			
			if(isset($query['id']))
			{
				$id = $query['id'];
			}
		
		}

		return $id;
	}

/**
*\brief récupère l'heure de début et l'heure de fin d'une plage horaire puis vérifie si la plage horaire 
*passée en paramètre chevauche une plage horaire existant déjà
*(lundi de 8h à 14h et lundi de 9h à 16h se chevauchent, mais lundi de 8h à 14h et mardi de 9h à 16h non)
*\return true si deux plages horaires se chevauchent, false sinon
*/
function overlap($timeslot_id,$day,$new_day,$start,$end)
	{
		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT start_time, end_time FROM time_slot WHERE id=:timeslot_id;");
			$query -> execute(array(
				"timeslot_id" => $timeslot_id
			));

			while($row = $query->fetch())
			{
				if($day == $new_day AND !($start > $row['end_time'] OR $end < $row['start_time']))
				{
					$overlap = true;
				}
			}
		}

		return $overlap;
	}

/**
*\brief récupère l'id des plages horaire ainsi que les jours correspondant à l'id du service
*puis fait appel à la fonction overlap
*\return true si au moins une des plage horaire du service dont l'id est passé en paramètre chevauche
*la plage horaire dont l'heure de début et l'heure fin sont passés en paramètre
*/
function overlapResult($service_id,$new_day,$start,$end)
	{
		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT time_slot, day FROM is_offered WHERE service=:service;");
			$query -> execute(array(
				"service" => $service_id
			));

			while($row = $query->fetch())
			{
				if(overlap($row['time_slot'],$row['day'],$new_day,$start,$end))
				{
					$overlap = true;
				}
			}
		}

		return $overlap;
	}

/**
*\brief ajoute une plage horaire à un service dont l'id est récupéré grâce au tableau $_POST
*en se servant des fonctions précédentes et en faisant les tests nécéssaires avant l'insertion dans la base de donnée
*\return une phrase indiquant si l'ajout a été effectué ou non
*/
function addTimeslot()
	{
		$result = null;

		$service_id = null;
		$day = null;

		$start_time = 0;
		$end_time = 0;
		$timeslot_id = -1;

		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null && isset($_SESSION['email']))
		{
			$service_id = (int)$_POST['add_id'];

			//on calcule les valeurs de start_time et end_time
			if(isset($_POST['day']))
			{
				$day = $_POST['day'];

				if(isset($_POST['start_hour']) && isset($_POST['start_minute']) && isset($_POST['end_hour']) && isset($_POST['end_minute']))
				{
					$start_time = ($_POST['start_hour'] * 60) + $_POST['start_minute'];
					$end_time = ($_POST['end_hour'] * 60) + $_POST['end_minute'];

				}
				
				$overlap = overlapResult($service_id,$day,$start_time,$end_time);

				if($end_time - $start_time >= 60)
				{
					if(!$overlap)
					{
						if(getTimeslotID($start_time,$end_time) == -1)
						{
							$query = $dbh -> prepare("INSERT INTO time_slot(start_time,end_time) VALUES (:start_time,:end_time)");
								
							$query -> execute(array(
								"start_time" => $start_time,
								"end_time" => $end_time
							));

						}
						if(getTimeslotID($start_time,$end_time) != -1)
						{
							$timeslot_id = getTimeslotID($start_time,$end_time);
							
							$query = $dbh -> prepare("INSERT INTO is_offered(time_slot,service,day) VALUES (:time_slot,:service,:day)");
									
							$query -> execute(array(
								"time_slot" => $timeslot_id,
								"service" => $service_id,
								"day" => $day
							));
						}

						$result .= "Votre horaire a bien été ajouté !";
					}
					else
					{
						$result .= "Votre horaire en chevauche un autre.";
					}
				}
				else
				{
					$result .= "Les horaires sont incohérents.";
				}
			}
			else
			{
				$result = "Pas de jour saisi";
			}

		}

		else
		{
			if(!isset($_SESSION['email']))
			{
				$result="Vous n'êtes pas connectés !";
			}
		}

		return $result;
	}