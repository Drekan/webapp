<?php
/**
*\brief Message lors de la déconnexion d'un utilisateur
*\return String
*
*
*
*/
function getLogoutMessage(){
		$return='A bientôt !<br/>';
		session_unset();
		return $return;
	}
