<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Connexion automatique à la base de données
*\return database handler
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief suppression dans la base de données d'un créneau d'un service
*\return
*
*
*
*/
function suppressionTimeslot($service_id, $timeslot_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM is_offered WHERE service=:service_id AND time_slot=:timeslot_id;");

			$query -> execute(array(
				"service_id" => $service_id,
				"timeslot_id" => $timeslot_id
			));
		}
	}

/**
*\brief suppression dans la base de donnée de toutes les occurences d'un service dans les emprunts de service
*\return
*
*
*
*/
function suppressionBorrowService($service_id, $timeslot_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM borrow_service WHERE service=:service_id AND time_slot=:timeslot_id;");

			$query -> execute(array(
				"service_id" => $service_id,
				"timeslot_id" => $timeslot_id
			));
		}
	}

/**
*\brief charge l'id d'un créneau donné par un temps de début et un temps de fin
*\return int, l'id du créneau correspondant dans la base de donnée, -1 sinon
*
*
*
*/
function getTimeslotID($start_time, $end_time)
	{
		$id = -1;
		$dbh = dbConnect();
		
		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT id FROM time_slot WHERE start_time=:start_time AND end_time=:end_time");
			$query->execute(array(
				"start_time" => $start_time,
				"end_time" => $end_time
				));
			$query=$query->fetch(PDO::FETCH_ASSOC);
			
			if(isset($query['id']))
			{
				$id = $query['id'];
			}
		
		}

		return $id;
	}


function overlap($timeslot_id,$day,$new_day,$start,$end)
	{
		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT start_time, end_time FROM time_slot WHERE id=:timeslot_id;");
			$query -> execute(array(
				"timeslot_id" => $timeslot_id
			));

			while($row = $query->fetch())
			{
				if($day == $new_day AND !($start > $row['end_time'] OR $end < $row['start_time']))
				{
					$overlap = true;
				}
			}
		}

		return $overlap;
	}

/**
*\brief Teste si un créneau proposé chevauche un autre créneau
*\return Booléen, false si les créneaux ne se chevauchent pas, true sinon
*
*
*
*/
function overlapResult($service_id,$new_day,$start,$end)
	{
		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("SELECT time_slot, day FROM is_offered WHERE service=:service;");
			$query -> execute(array(
				"service" => $service_id
			));

			while($row = $query->fetch())
			{
				if(overlap($row['time_slot'],$row['day'],$new_day,$start,$end))
				{
					$overlap = true;
				}
			}
		}

		return $overlap;
	}

/**
*\brief fonction qui gère le changement de créneaux d'un service déjà mis en ligne par un utilisateur
*\return String, information sur la bonne exécution de la requête
*
*
*
*/
function getChangeTimeslotResult()
	{
		$result = null;

		$is_offered_id = null;
		$service_id = null;
		$timeslot_id = null;
		$day = null;

		$start_time = 0;
		$end_time = 0;
		$new_timeslot_id = -1;

		$overlap = false;

		$dbh = dbConnect();

		if($dbh != null && isset($_SESSION['email']) && isset($_POST['timeslot_id']))
		{
			$is_offered_id = $_POST['timeslot_id'];

			//$result .= $is_offered_id;

			$list = explode("/",$is_offered_id);

			$timeslot_id = (int)$list[0];
			$service_id = (int)$list[1];

			//on commence par supprimer l'ancien horaire
			suppressionTimeslot($service_id,$timeslot_id);

			//on supprime l'emprunt concernant cet horaire
			suppressionBorrowService($service_id,$timeslot_id);

			//on calcule les valeurs de start_time et end_time
			if(isset($_POST['day']))
			{
				$day = $_POST['day'];

				if(isset($_POST['start_hour']) && isset($_POST['start_minute']) && isset($_POST['end_hour']) && isset($_POST['end_minute']))
				{
					$start_time = ($_POST['start_hour'] * 60) + $_POST['start_minute'];
					$end_time = ($_POST['end_hour'] * 60) + $_POST['end_minute'];

				}

				$overlap = overlapResult($service_id,$day,$start_time,$end_time);

				if($end_time - $start_time >= 60)
				{
					if(!$overlap)
					{
						if(getTimeslotID($start_time,$end_time) == -1)
						{
							$query = $dbh -> prepare("INSERT INTO time_slot(start_time,end_time) VALUES (:start_time,:end_time)");
								
							$query -> execute(array(
								"start_time" => $start_time,
								"end_time" => $end_time
							));

						}
						if(getTimeslotID($start_time,$end_time) != -1)
						{
							$new_timeslot_id = getTimeslotID($start_time,$end_time);

							$query = $dbh -> prepare("INSERT INTO is_offered(time_slot,service,day) VALUES (:time_slot,:service,:day)");
								
							$query -> execute(array(
								"time_slot" => $new_timeslot_id,
								"service" => $service_id,
								"day" => $day
							));

						}

						$result .= "Votre horaire a bien été modifié !";
					}
					else
					{
						$result .= "Votre horaire en chevauche un autre.";
					}
				}
				else
				{
					$result .= "Les horaires sont incohérents";
				}
			}
			else
			{
				$result .= "Pas de jour saisi";
			}

		}

		else
		{
			if(!isset($_SESSION['email']))
			{
				$result="Vous n'êtes pas connectés !";
			}
		}

		return $result;
	}

?>