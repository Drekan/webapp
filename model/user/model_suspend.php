<?php
/**
*\author SEGURA Bastien
*/

/**
*\brief insère les valeurs correspondant à une suspension de compte dans la base de donnée
*\return une phrase qui indique si l'insertion s'est bien passée ou non
*/
function sendSuspension(){
	$result='';
	if(isset($_POST['email'])){
		try{
			$duration=0;
			if(intval($_POST['duration']) > 0){
				$duration=intval($_POST['duration']) * 86400;
			}
			else{
				$duration=86400*365*100; //un siècle de malédiction
			}
			require('../secret.php');
			$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
			$query=$dbh->prepare("insert into suspended(email,suspention_date,suspention_duration) values (:email,:date,:duration)");
			$query->execute(array(
				"email" => $_POST['email'],
				"date" => time(),
				"duration" => $duration
			));
			$result='Le compte a bien été suspendu.';
		}
		catch(PDOException $e){
			echo $e->getMessage() . '<br> \n';
			$result='Erreur lors de la procédure.';
		}
	}

	return $result;
}
