<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Se connecte automatiquement à la base de données
*\return database holder qui permet d'accèder à la base
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief Suppression de toutes les références d'un bien dans la table borrow_good
*\return 
*
*
*
*/
function suppressionAllBorrowGoodOffered($good_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionBorrowGoodOffered = $dbh -> prepare("DELETE FROM borrow_good WHERE good=:id;");

			$suppressionBorrowGoodOffered -> execute(array(
				"id" => $good_id
			));
		}
	}

/**
*\brief Suppression de toutes les références d'un service dans la table is_offered
*\return
*
*
*
*/
function suppressionAllTimeslot($service_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM is_offered WHERE service=:service_id;");

			$query -> execute(array(
				"service_id" => $service_id
			));
		}
	}

/**
*\brief Suppression de toutes les références d'un service dans la table borrow_service
*\return
*
*
*
*/
function suppressionAllBorrowServiceOffered($service_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionBorrowServiceOffered = $dbh -> prepare("DELETE FROM borrow_service WHERE service=:service_id;");

			$suppressionBorrowServiceOffered -> execute(array(
				"service_id" => $service_id
			));
		}
	}

/**
*\brief Supprime tous les biens postés par un utilisateur donné
*\return
*
*
*
*/
function suppressionAllGood($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionGood = $dbh -> prepare("DELETE FROM good WHERE email_member=:email;");

			$suppressionGood -> execute(array(
				"email" => $email
			));
		}

	}

/**
*\brief Supprime tous les services postés par un utilisateur donnée
*\return
*
*
*
*/
function suppressionAllService($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionService = $dbh -> prepare("DELETE FROM service WHERE email_member=:email;");

			$suppressionService -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief Supprime un utilisateur donné du site
*\return
*
*
*
*/
function suppressionMember($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionMember = $dbh -> prepare("DELETE FROM member WHERE email=:email;");

			$suppressionMember -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief suppression de tous les emprunts de bien d'un utilisateur donné
*\return
*
*
*
*/
function suppressionAllBorrowGoodUse($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionBorrowGoodUse = $dbh -> prepare("DELETE FROM borrow_good WHERE email_member=:email;");

			$suppressionBorrowGoodUse -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief suppression de tous les emprunts de service d'un utilisateur donné
*\return
*
*
*
*/
function suppressionAllBorrowServiceUse($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionBorrowServiceUse = $dbh -> prepare("DELETE FROM borrow_service WHERE email_member=:email;");

			$suppressionBorrowServiceUse -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief Suppression de toutes les notifications d'un utilisateur donné
*\return
*
*
*
*/
function suppressionAllNotification($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionNotification = $dbh -> prepare("DELETE FROM notification WHERE receiver=:email;");

			$suppressionNotification -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief teste si l'utilisateur est un admin ou non
*\return booléen, true si l'utilisateur est un admin, false sinon
*
*
*
*/
function isAdmin($email)
	{
		$dbh = dbConnect();
		$result = false;

		if($dbh != null)
		{
			$isAdmin = $dbh -> prepare("select * from admin where email=:email");

			$isAdmin -> execute(array(
				"email" => $email
			));

			$fetch = $isAdmin -> fetchAll();

			if(isset($fetch[0]))
			{
				$result = true;
			}
		}

		return $result;
	}

/**
*\brief Supprime un admin donné de la base de donnée
*\return
*
*
*
*/
function suppressionAdmin($email)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppressionAdmin = $dbh -> prepare("DELETE FROM admin WHERE email=:email;");

			$suppressionAdmin -> execute(array(
				"email" => $email
			));
		}
	}

/**
*\brief Supprime le compte qui est actuellement connecté
*\return String, le résultat de la suppression
*
*
*
*/
function suppressionAccount()
	{
		$email = $_SESSION['email'];

		$result = null;

		$dbh = dbConnect();

		if($dbh != null)
		{
			//On récupère les id des biens de ce compte
			$getGoodId = $dbh -> prepare("SELECT id from good where email_member=:email;");

			$getGoodId -> execute(array(
				"email" => $email
			));

			//on supprime tous les emprunts liés aux biens proposé par ce compte
			while($row = $getGoodId -> fetch())
			{
				suppressionAllBorrowGoodOffered($row['id']);
			}

			//On récupère les id des services de ce compte
			$getGoodService = $dbh -> prepare("SELECT id from service where email_member=:email;");

			$getGoodService -> execute(array(
				"email" => $email
			));

			//On supprime tous les emprunts ainsi que toutes les plages horaires liés aux services proposés par ce compte
			while($row = $getGoodService->fetch())
			{
				suppressionAllTimeslot($row['id']);
				suppressionAllBorrowServiceOffered($row['id']);
			}

			//On supprime les emprunts de bien effectué par ce compte
			suppressionAllBorrowGoodUse($email);

			//On supprime les emprunts de service effectué par ce compte
			suppressionAllBorrowServiceUse($email);

			//On supprime les biens
			suppressionAllGood($email);

			//On supprime les services
			suppressionAllService($email);

			//On supprime les notifications
			suppressionAllNotification($email);

			if(isAdmin($email))
			{
				suppressionAdmin($email);
			}

			//Et enfin, on supprime le compte
			suppressionMember($email);

			$result .= "Le compte a bien été supprimé";
			session_unset();
		}
		else
		{
			$result .= "Suppression impossible, vous n'êtes pas connecté.";
		}

		return $result;
	}
