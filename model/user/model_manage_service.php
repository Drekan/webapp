<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief chargement des services de l'utilisateur afin de pouvoir les modifier
*\return String, html des services de l'utilisateur
*
*
*
*/
function displayManageService()
	{
		$display = null;
		$email = null;
		$id = null;

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];
		}


		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

			$id = $_POST['change_id'];
			
			$query = $dbh -> prepare("SELECT id, title, description, price
						FROM service
						WHERE email_member=:email
								AND id=:id;");

			$query -> execute(array(
				"email" => $email,
				"id" => $id
			));

			while($row = $query->fetch())
			{
				$display .= '<form method="post" action="manage_service_change_controller.php" id="manage_service_change_form" class="manage_service">';
				
				$display .= '<label for = "title">';
				$display .= '*Titre : <input type="text" name = "title" value="'.$row['title'].'"required/>';
				$display .= '</label><br/>';
				
				$display .= '<label for = "description">';
				$display .= 'Description : <textarea name = "description" maxlength = "180" form = "manage_service_change_form">'.$row['description'].'</textarea>';
				$display .= '</label><br/>';
				
				$display .= '<label for="price">';
				$display .= '*Prix à la journée : <input type="number" name="price" value="'.$row['price'].'"required/>';
				$display .= '</label><br/>';
				
				$display .= '<button type="submit" value="'.$row['id'].'" name="service_change_id">Valider les modifications</button>';
				
				$display .= '</form>';


				$display .= '<form method="post" action="manage_timeslot_choice_controller.php" id="timeslot_manage_form" class="noborder">';
				
				$display .= '<button type="submit" value="'.$row['id'].'" name="timeslot_manage_id">Gérer les horaires</button>';
				
				$display .= '</form>';

			}
		}

		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $display;
	}
?>
