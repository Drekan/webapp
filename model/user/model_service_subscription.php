<?php 
date_default_timezone_set('Europe/Paris');
function isAvailable($timeslot_id,$day,$service_id){
	$available=true;
	try{
		require('../secret.php');
		$dbh= new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		$query = $dbh -> prepare("select * from borrow_service 
			where service=:service and time_slot=:timeslot and day=:day");
		$query -> execute(array(
			"service" => $service_id,
			"timeslot" => $timeslot_id,
			"day" => $day
		));
		if($query->rowCount()!=0){
			$available=false;
		}
	}
	catch(PDOException $e){
		echo $e->getMessage() . "<br/> \n";
	}

	return $available;
}

function nextTimeslot($day){ //return the time(timestamp) remaining between now and the day given in parameters
	$day_value=-1;
	switch($day){
	case "Lundi":
		$day_value=0;
		break;
	case "Mardi":
		$day_value=1;
		break;
	case "Mercredi":
		$day_value=2;
		break;
	case "Jeudi":
		$day_value=3;
		break;
	case "Vendredi":
		$day_value=4;
		break;
	case "Samedi":
		$day_value=5;
		break;
	case "Dimanche":
		$day_value=6;
		break;
	}

	$current_day_value=(intval(date('w'))+6)%7;

	$offset=0;

	if(($day_value - $current_day_value) < 0){ //next timeslot is next week
		$offset=7+($day_value - $current_day_value);
	}
	else{
		$offset=$day_value - $current_day_value;
	}
	$time=time();
	return ( (int)($time - $time % 86400) + ($offset * 86400));
}

function frenchDay($number){
	$return='';
	switch($number){
	case 0:
		$return='Dimanche';
		break;
	case 1:
		$return='Lundi';
		break;
	case 2:
		$return='Mardi';
		break;
	case 3:
		$return='Mercredi';
		break;
	case 4:
		$return='Jeudi';
		break;
	case 5:
		$return='Vendredi';
		break;
	case 6:
		$return='Samedi';
		break;
	}
	return $return;
}

function getPrice($service_id,$start,$end)
{
	$price = 0;
	try
	{
		require('../secret.php');
		$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

		$query = $dbh -> prepare("SELECT price FROM service WHERE id=:service_id;");

		$query -> execute(array(
			"service_id" => $service_id
		));

		$query = $query -> fetch(PDO::FETCH_ASSOC);

		if(isset($query['price']))
		{
			$price = $query['price'];
		}

		$hour = ($end - $start)/60;

		$price = $price * $hour;
	}
	catch(PDOException $e)
	{
		echo $e->getMessage() . "<br/> \n";
	}

	return $price;
}

function printDate($timestamp,$start,$end,$service_id){
	$price = getPrice($service_id,$start,$end);

	$return='';
	$return.=frenchDay(intval(date('w',$timestamp))) . ' ';
	$return.=date('d / m / Y',$timestamp);
	$return.=' de ' . (int)($start / 60) . 'h' . (($start % 60)==0?'00':$start % 60) . ' à ';
	$return.=(int)($end / 60) . 'h' . (($end%60)==0?'00':$end%60);
	$return.='&emsp;&emsp;&emsp;(prix : '.$price."€)";

	return $return;
}

function minArray($array,$already_seen){
	$i=0;
	while($already_seen[$i]!=0){
		$i++;
	}
	$min=$i;
	for($i=0;$i<count($array);$i++){
		if($array[$i] < $array[$min] AND $already_seen[$i]!=1){
			$min=$i;
		}
	}
	return $min;
}

function fillTimeslotForm(){
	$result='';
	$number_of_week=2;
	if(isset($_POST['service_id'])){
		$service_id=$_POST['service_id'];
		setcookie('service_id',$service_id);

		try{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);		
			$query = $dbh -> prepare("select * from is_offered,time_slot
				where time_slot=id and service=:service");
			$query -> execute(array(
				"service" => $service_id
			));

			$number_of_timeslot=0;
			$next_timestamp=0;
			$option_value='';
			$first_copy_done=false;

			$timeslot_array=array();
			$row_array=array();
			for($i=0;$i<$number_of_week;$i++){
				if(!$first_copy_done){
					$copy=$query->fetchAll();
					$first_copy_done=true;
				}
				$copy2=$copy;	
				foreach($copy2 as $row){
					$next_timestamp=nextTimeslot($row['day']);
					if(isAvailable($row['id'],(86400 * 7 *$i) + $next_timestamp+((int)intval($row['start_time']))*60 -3600,$row['service'])){
						array_push($timeslot_array,(86400 * 7 *$i) + $next_timestamp);
						array_push($row_array,$row);
							/*$option_value.=$row['id'] . ':' . ((86400 * 7 *$i) +  $next_timestamp +((int)intval($row['start_time']))*60) ;
							$print_date=printDate((86400 * 7 * $i) + $next_timestamp,intval($row['start_time']),intval($row['end_time']));
							$result.='<option value="' . $option_value . '">' . $print_date . '</option>';*/
						$number_of_timeslot++;
					}
				}



			}

			$option_counter=0;

			$index_seen=array();
			for($i=0;$i<count($timeslot_array);$i++){
				$index_seen[$i]=0;
			}
			$index_max=0;
			while($option_counter<count($timeslot_array)){
				//print_r($index_seen);
				//echo '<br>';
				$option_value='';
				$index_min=minArray($timeslot_array,$index_seen);
				$option_value.=$row_array[$index_min]['id'] . ':' . ($timeslot_array[$index_min] +((int)intval($row_array[$index_min]['start_time']))*60 - 3600) ;
				$print_date=printDate($timeslot_array[$index_min],intval($row_array[$index_min]['start_time']),intval($row_array[$index_min]['end_time']),$service_id);
				$result.='<option value="' . $option_value . '">' . $print_date . '</option>';
				$index_seen[$index_min]=1;
				$option_counter++;
				//echo $row_array[$index_min]['start_time'] . '<br>';
			}
			//print_r($row_array);

			if($number_of_timeslot==0){
				$result=0;
			}
		}
		catch(PDOException $e){
			echo $e->getMessage() . "<br/> \n";
		}

	}

	return $result;
}
