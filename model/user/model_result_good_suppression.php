<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief connexion automatique à la base de données
*\return database handler permettant d'accéder à la base de donnés
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief Suppression dans la base de données des emprunts d'un bien donné
*\return 
*
*
*
*/
function suppressionBorrowGood($good_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$suppression_borrow = $dbh -> prepare("DELETE FROM borrow_good WHERE good=:id;");

			$suppression_borrow -> execute(array(
				"id" => $good_id
			));
		}
	}

/**
*\brief Suppression dans la base de données d'un bien
*\return String, information sur le succès ou non de la connexion
*
*
*
*/
function SuppressionGood()
	{

		$result = '';
		$email = null;

		$id = null;
		$dbh = dbConnect();

		if(isset($_SESSION['email']))
		{
			if($dbh != null)
			{
				//on récupère l'id correspondant au bien que l'on veut modifier
				$id = $_POST['suppression_good_id'];

				suppressionBorrowGood($id);

				$suppression_good = $dbh -> prepare("DELETE FROM good WHERE id=:id;");

				$suppression_good -> execute(array(
						"id" => $id
					));

				$result .= "Votre suppression de bien a bien été prise en compte !";
			}
			else
			{
				$result .= "Problème lors de la connection à la base de donnée, la suppression n'a pas pu être effectuée.";
			}
		}
		else
		{
			$result .= "Vous n'êtes pas connecté !";
		}

		return $result;
	}
?>