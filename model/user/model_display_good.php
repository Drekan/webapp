<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Charge tous les biens disponibles dans la base de données
*\return String, les bien structurés en html
*
*
*
*/
function displayGood()
	{
		$display = null;

		$search = null;
		
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
			
			if(isset($_POST['search_word']))
			{
				$buf = $_POST['search_word'];

				$list = explode(" ",$buf);

				$search = $list[0];
				
				$query = "SELECT good.id goodID,title, description, price, image_link, last_name, first_name, email
							FROM good, member
							WHERE email_member = email 
								AND title like '%".addcslashes($search,"'")."%'
							ORDER BY id DESC;";
			}
			else
			{
				$query = "SELECT good.id goodID,title, description, price, image_link, last_name, first_name, email
							FROM good, member
							WHERE email_member = email ORDER BY id DESC;";
			}
			
			foreach($dbh -> query($query) as $row)
			{
				$display .= '<form method="post" action="good_subscription_controller.php" id="display_good_form" class="display_good">';
				$display .= '<article>';
				$display .= '<img style="max-width: 150px; height: auto;" src="'.$row['image_link'].'"/>';
				$display .= '<p>'.$row['title'].'</p>';
				$display .= '<p>'.$row['last_name'].'&nbsp;'.$row['first_name'].'</p>';
				$display .= '<div class="description">'.$row['description'].'</div><br/>';
				$display .= '<p class="price">'.$row['price'].'€/jour</p>';
				$display .= '<div class="submit">';
				if(isset($_SESSION['email']) AND $_SESSION['email'] != $row['email']){
					$display .= '<button type="submit" value="'. $row['goodID'] .'" name="good_id">emprunter</button>';
				}
				$display .= '</div>';
				$display .= '</article>';
				$display .= '<br/>';
				$display .= '</form>';
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $display;
	}
?>
