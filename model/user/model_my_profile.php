<?php
/**
*\brief Chargement dans la base de données de l'état de la cagnotte de l'utilisateur connecté
*\return String, html correspondant à la cagnotte de l'utilisateur
*
*
*
*/
function getJackpotInfo(){
	$jackpot_info='';

	try{
		require('../secret.php');
		$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

		$query=$dbh->prepare("SELECT * from member where email=:email");
		$query->execute(array("email" => $_SESSION['email']));
		
		$result=$query->fetchAll();
		$earned=$result[0]['money_earned'];
		$spent=$result[0]['money_spent'];
		
		$jackpot_info.='<div class="profile_jackpot"><h1>Ma cagnotte</h1>';
		$jackpot_info.='<div class="value_money"><div  class="profile_money_earned"><p>Argent gagné : <b>' . $earned . '€</b></p><p> (' . $result[0]['bs_offered']  . ' biens prêtés ou services rendus)</p></div>';
		$jackpot_info.='<div class="profile_money_spent"><p>Argent dépensé : <b>' . $spent . '€</b></p><p> (' . $result[0]['bs_used'] . ' biens ou services sollicités)</p></div>';

		$jackpot_info.="</div>";

		if($earned > 2*$spent){
			$jackpot_info.='<p>Hm, ça commence à faire un paquet d\'argent ! Nous vous conseillons d\'en dépenser un peu ;) </p>';
		}
		else if($spent > 2*$earned){
			$jackpot_info.='<p><em>Sans vouloir juger, ça fait pas mal de dépenses, non ?... Il serait temps de gagner un peu d\'argent ;)</em></p>';
		}

		$jackpot_info.="</div>";
		
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}

	return $jackpot_info;
}

