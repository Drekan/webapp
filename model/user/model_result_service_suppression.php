<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief
*\return
*
*
*
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief
*\return
*
*
*
*/
function suppressionAllTimeslot($service_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM is_offered WHERE service=:service_id;");

			$query -> execute(array(
				"service_id" => $service_id
			));
		}
	}

/**
*\brief
*\return
*
*
*
*/
function suppressionAllBorrowService($service_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM borrow_service WHERE service=:service_id;");

			$query -> execute(array(
				"service_id" => $service_id
			));
		}
	}

/**
*\brief
*\return
*
*
*
*/
function suppressionService()
	{

		$result = '';
		$email = null;

		$service_id = null;

		$dbh = dbConnect();

		if(isset($_SESSION['email']))
		{
			if($dbh != null)
			{
				$service_id = $_POST['suppression_id'];

				//on supprime tous les horaires liés à ce service
				suppressionAllTimeslot($service_id);

				//on supprime tous les emprunts concernant ce service
				suppressionAllBorrowService($service_id);

				$suppression_service = $dbh -> prepare("DELETE FROM service WHERE id=:id;");

				$suppression_service -> execute(array(
						"id" => $service_id
					));

				$result .= "Votre suppression de service a bien été prise en compte !";
			}
			else
			{
				$result .= "Problème lors de la connection à la base de donnée, la suppression n'a pas pu être effectuée.";
			}
		}
		else
		{
			$result .= "Vous n'êtes pas connecté !";
		}

		return $result;
	}
?>