<?php
/**
*\brief connexion automatique à la base de données
*\return database handler permettant d'accéder à la bdd
*
*
*
*/
function getDBH(){
	require('../secret.php');
	$dbh=new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
	return $dbh;
}
/**
*\brief teste si un utilisateur donnée est un administrateur ou non
*\return Booléen, true si l'utilisateur est un admin, false sinon
*
*
*
*/
function isAdmin($email){
	$return=false;
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select * from admin where email=:email");
		$query->execute(array("email" => $email));
		$fetch=$query->fetchAll();
		if(isset($fetch[0])){
			$return=true;
		}
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
	return $return;
}
/**
*\brief chargement des avertissements générés par la base de données
*\return String, html correspondant aux avertissements présents dans la bdd
*
*
*
*/
function getWarningDisplay($email){
	$result='<h2>Avertissements</h2>';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select * from notification where notif_type='warning'");
		$query->execute();
		foreach($query as $row){
			if($email != $row['receiver']){
				$result.='<p><a href="see_profile_controller.php?email=' . $row['receiver'] . '">' . $row['receiver']. '</a> : ' . $row['message'] . '</p>';
			}
		}
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
	return $result;
}

/**
*\brief Suppression automatique des notifications qui ne sont plus d'actualité
*\return
*
*
*
*/
function suppressionOldNotif()
{
	$dbh = getDBH();

	$now = time();

	if($dbh != null)
	{
		$query = $dbh -> prepare("DELETE FROM notification WHERE date_borrow<:now;");
		$query -> execute(array(
			"now" => $now
		));
	}
}

/**
*\brief Chargement dans la base de données des notifications relatives à l'utilisateur connecté
*\return String, html correspondant aux notifications
*
*
*
*/
function getInfoDisplay(){
	$result='<h2>Notifications</h2>';
	try{
		$dbh=getDBH();
		$query=$dbh->prepare("select * from notification where notif_type='information' ORDER BY id DESC");
		$query->execute();
		foreach($query as $row){
			if($row['receiver']==$_SESSION['email']){
				$result.='<p>' .  $row['message'] . '</p>';
			}
		}
	}
	catch(PDOException $e){
		echo $e->getMessage() . '<br> \n';
	}
	return $result;

}

/**
*\brief fonction générale qui gère l'affichage des notifications et, si besoin, des warning
*\return String, html de la page de notification
*
*
*
*/
function getNotificationDisplay(){
	$result='';
	$email = null;

	if(isset($_SESSION['email']))
	{
		$email = $_SESSION['email'];
		
		if(isAdmin($email))
		{
			$result .= getWarningDisplay($email);
		}
		suppressionOldNotif();
		$result .= getInfoDisplay();
	}

	return $result;
}
