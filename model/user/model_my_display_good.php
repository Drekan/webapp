<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Affichage des bien de l'utilisateur dans son profil
*\return String, html correspondant aux biens postés par l'utilisateur connecté
*
*
*
*/
function displayMyGood()
	{
		$display = null;
		$email = null;

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];

			try
			{
				require('../secret.php');
				$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

				$query = $dbh -> prepare("SELECT id, title, description, price, image_link
							FROM good
							WHERE email_member=:email
							ORDER BY id DESC");
				$query -> execute(array(
					"email" => $email
				));

				while($row = $query->fetch())
				{				
					$display .= '<form method="post" action="manage_good_controller.php" id="display_my_good_form" class="display_good">';

					$display .= '<article>';
					$display .= '<img style="max-width: 150px; height: auto;" src="'.$row['image_link'].'"/>';
					$display .= '<p>'.$row['title'].'</p>';
					$display .= '<div class="description">'.$row['description'].'</div><br/>';
					$display .= '<p class="price">'.$row['price'].'€/jour</p>';
					$display .= '<div class="submit">';
					$display .= '<button type="submit" value="'.$row['id'].'" name="good_id">Modifier</button>';
					$display .= '</div>';
					$display .= '<div class="submit">';
					$display .= '<button type="submit" value="'.$row['id'].'" name="suppression_good_id">Supprimer</button>';
					$display .= '</div>';
					$display .= '<br/>';
					$display .= '</article>';

					$display .= '</form>';

					
					//$display .= '<form method="post" action="#" id="suppression_my_good" class="noborder">';
					
					//$display .= '<article>';
					//$display .= '<button type="submit" value="'.$row['id'].'" name="suppression_good_id">Supprimer</button>';
					//$display .= '</article>';
					
					//$display .= '</form>';
					//echo $buf_title;
				}
			}
			catch(PDOException $e)
			{
				echo $e->getMessage()."<br/>\n";
				//die("Connexion impossible !");
			}
		}
		else
		{
			$display .= "Vous n'êtes pas connecté !";
		}

		return $display;
	}
?>