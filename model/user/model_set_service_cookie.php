<?php

/**
*\brief
*\return
*
*
*
*/
function load_user_info(){
		if(isset($_POST['title'])){
			setcookie('title',$_POST['title']);
		}
		
		if(isset($_POST['description'])){
			setcookie('description',$_POST['description']);
		}
		
		if(isset($_POST['price'])){
			setcookie('price',$_POST['price']);
		}
		
		if(isset($_POST['title'])){
			setcookie('title',$_POST['title']);
		}	
	}
	
/**
*\brief
*\return
*
*
*
*/
function overlap($array,$day,$start,$end){
		$overlap=false;
		foreach($array as $row){
			if($row[3]==true){
				if($day==$row[0] AND !($start > $row[2] OR $end < $row[1])){
					$overlap=true;
				}
			}
		}
		return $overlap;
	}

/**
*\brief
*\return
*
*
*
*/
function loadCookie(){

		load_user_info();	

		if(isset($_POST['start_hour'])){

			$start_total_min=(60 * $_POST['start_hour']) + $_POST['start_minute'];
			$end_total_min=(60 * $_POST['end_hour']) + $_POST['end_minute'];	
			$day=$_POST['day'];

			if(($end_total_min - $start_total_min) >= 60){	
				if(isset($_COOKIE['timeslot'])){	

					$timeslot_array=unserialize($_COOKIE['timeslot']);

					if(!overlap($timeslot_array,$day,$start_total_min,$end_total_min)){
						$timeslot_array[]=array($day,$start_total_min,$end_total_min,true);
					}
					setcookie('timeslot',serialize($timeslot_array));
				}
				else{
					$timeslot_array=array(array($day,$start_total_min,$end_total_min,true));
					setcookie('timeslot',serialize($timeslot_array));
				}
			}
			else{
				//ici le dernier créneau ajouté n'est pas cohérent			
			}
		}
	}

/**
*\brief
*\return
*
*
*
*/
function displayMinute($value){
		return ($value==0?'00':$value);
	}

/**
*\brief
*\return
*
*
*
*/
function numberDeleted($timeslot_array){
		$number_deleted=0;
		if(isset($timeslot_array[0])){
			foreach($timeslot_array as $timeslot){
				if($timeslot[3]==false){
					$number_deleted++;
				}
			}
		}
		return $number_deleted;
	}

/**
*\brief
*\return
*
*
*
*/
function allDeleted($timeslot_array){
		return count($timeslot_array)==numberDeleted($timeslot_array);
	}

	//previous timeslot are displayed her
/**
*\brief
*\return
*
*
*
*/
function timeslotDisplayArray($timeslot_array){
		$result='';
		$index=0;
		$delete_index=-1;
		if(!allDeleted($timeslot_array)){
			if(isset($_POST['delete_timeslot'])){
				$delete_index=$_POST['delete_timeslot'];
			}
			ob_start();
			echo '<form method="post" action="timeslot_form_controller.php" class="historique_timeslot">';
			foreach($timeslot_array as $timeslot){
				if($index != $delete_index AND ($timeslot[3]==true)){
					echo '<div><p>' . $timeslot[0] . ' de ' . (int)($timeslot[1]/60) . 'h' . displayMinute($timeslot[1]%60) . ' à ' . (int)($timeslot[2]/60) . 'h' . displayMinute($timeslot[2]%60);
					echo '</p><button type="submit" value="' . $index . '" name="delete_timeslot">Supprimer ce créneaux</button></div>';
				}
				$index++;
			}
			$result=ob_get_clean();
		}
		return $result;
	}



/**
*\brief
*\return
*
*
*
*/
function getPreviousTimeslot(){
		$result='';
		$cookie_timeslot='';
		if(isset($_COOKIE['timeslot'])){
			if(isset($_POST['delete_timeslot'])){
				$delete_index=$_POST['delete_timeslot'];
				$cookie_timeslot=unserialize($_COOKIE['timeslot']);
				$cookie_timeslot[$delete_index][3]=false;
				setcookie('timeslot',serialize($cookie_timeslot));
			}
			else{
				$cookie_timeslot=unserialize($_COOKIE['timeslot']);
			}
			if(count($cookie_timeslot) != 0){
				$result=timeslotDisplayArray($cookie_timeslot);
			}
		}
		if(isset($_POST['day']) AND (($_POST['end_minute']+60*$_POST['end_hour']) - ($_POST['start_minute']+60*$_POST['start_hour']) >=60 )){

			$start_total_min=$_POST['start_hour']*60+$_POST['start_minute'];
			$end_total_min=$_POST['end_hour']*60+$_POST['end_minute'];
			$day=$_POST['day'];
			//if the cookie exists and the new timeslot doesn't overlap the previous ones, it's displayed
			if(isset($_COOKIE['timeslot']) AND count($cookie_timeslot) != 0){
				$index=count($cookie_timeslot);
				if(!overlap($cookie_timeslot,$day,$start_total_min,$end_total_min)){
					ob_start();
					if(allDeleted($cookie_timeslot)){
						echo '<form method="post" action="timeslot_form_controller.php" class="historique_timeslot">';
					}
					echo '<div><p>' . $_POST['day'] . ' de ' . $_POST['start_hour'] . 'h' . displayMinute($_POST['start_minute']) . ' à ' . $_POST['end_hour'] . 'h' . displayMinute($_POST['end_minute']) . '</p><button type="submit" value="' . $index . '" name="delete_timeslot">Supprimer ce créneaux</button></div></form>';
					$result.=ob_get_clean();
				}
				else{
					$result='<p class="erreur_timeslot">Le créneau sélectionné en chevauche d\'autres</p>' . $result . '</form>';
				}
			}
			else{
				$result= '<form method="post" action="timeslot_form_controller.php" class="historique_timeslot"><div><p>' . $_POST['day'] . ' de ' . $_POST['start_hour'] . 'h' . displayMinute($_POST['start_minute']) . ' à ' . $_POST['end_hour'] . 'h' . displayMinute($_POST['end_minute']) . '</p><button type="submit" value="0" name="delete_timeslot">Supprimer ce créneaux</button></div></form>';
			}

		}
		else if(isset($_POST['day'])){
			$result='<p class="erreur_timeslot">Attention : le créneau sélectionné n\'était pas cohérent</p>' . $result . '</form>';
		}

		if(isset($_POST['delete_timeslot'])){
			if(isset($_COOKIE['timeslot']) AND (numberDeleted(unserialize($_COOKIE['timeslot']) >= count(unserialize($_COOKIE['timeslot']))-1)) AND count(unserialize($_COOKIE['timeslot']))!=0 ){
			}
			else{
				if(!isset($_COOKIE['timeslot']) OR count(unserialize($_COOKIE['timeslot']))!=0){
					$result.='</form>';
				}
			}
		}

		return $result;
	}




