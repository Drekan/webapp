<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief charge la gestion des biens
*\return String, html des biens de l'utilisateur
*
*
*
*/
function displayManageGood()
	{
		$display = null;
		$email = null;
		$id = null;

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];
		}


		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

			$id = $_POST['good_id'];
			
			$query = $dbh -> prepare("SELECT id, title, description, price, image_link
						FROM good
						WHERE email_member=:email
								AND id=:id;");

			$query -> execute(array(
				"email" => $email,
				"id" => $id
			));

			while($row = $query->fetch())
			{
				$display .= '<form method="post" action="manage_good_change_controller.php" id="manage_good_change_form" class="manage_good">';
				
				$display .= '<label for = "title">';
				$display .= '*Titre : <input type="text" name = "title" value="'.$row['title'].'"required/>';
				$display .= '</label><br/>';
				
				$display .= '<label for = "description">';
				$display .= 'Description : <textarea name = "description" maxlength = "180" form = "manage_good_change_form">'.$row['description'].'</textarea>';
				$display .= '</label><br/>';
				
				$display .= '<label for="image_link">';
				$display .= 'Lien de l\'image : <input type="text" name = "image_link" maxlength = "2083" value = "'.$row['image_link'].'"/>';
				$display .= '</label><br/>';
				
				$display .= '<label for="price">';
				$display .= '*Prix à la journée : <input type="number" name="price" value="'.$row['price'].'"required/>';
				$display .= '</label><br/>';
				
				$display .= '<button type="submit" value="'.$row['id'].'" name="good_change_id">Modifier</button>';
				
				$display .= '</form>';

			}
		}

		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $display;
	}
?>