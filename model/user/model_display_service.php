<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Charge tous le services disponibles dans la base de données
*\return String, html correspondant à l'affichage des services
*
*
*
*/
function displayService()
	{
		$display = null;
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

			if(isset($_POST['search_word']))
			{
				$buf = $_POST['search_word'];

				$list = explode(" ",$buf);

				$search = $list[0];

				$query = "SELECT service.id serviceID, title, description, price, last_name, first_name, email
							FROM service, member
							WHERE email_member = email 
								AND title like '%".addcslashes($search,"'")."%'
							ORDER BY id DESC;";
			}
			else
			{
				$query = "SELECT service.id serviceID, title, description, price, last_name, first_name, email
							FROM service, member
							WHERE email_member = email ORDER BY id DESC;";
			}

			foreach($dbh -> query($query) as $row)
			{
				$display .= '<form method="post" action="service_subscription_controller.php" id="display_service_form" class="display_service">';
				$display .= '<article>';
				$display .= '<p>'.$row['title'].'</p>';
				$display .= '<p>'.$row['last_name'].'&nbsp;'.$row['first_name'].'</p>';
				$display .= '<div class="description">'.$row['description'].'</div><br/>';
				$display .= '<p class="price">'.$row['price'].'€/heure</p>';
				$display .= '<div class="submit">';
				if(isset($_SESSION['email']) AND $_SESSION['email'] != $row['email']){
					$display .= '<button type="submit" value="'. $row['serviceID'] . '" name="service_id">Demander</button>';
				}
				$display .= '</div>';
				$display .= '</article>';
				$display .= '<br/>';
				$display .= '</form>';
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $display;
	}
?>
