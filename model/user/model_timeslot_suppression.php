<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\author DE BAERE Estelle
*/

/**
*\brief connexion à la base de donnée
*\return si la connexion fonctionne ça retourne un database handle
*/
function dbConnect()
	{
		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $dbh;
	}

/**
*\brief supprime la plage horaire proposée dont l'id du service et l'id du timeslot
*tous deux passés en paramètre
*/
function suppressionTimeslot($service_id, $timeslot_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM is_offered WHERE service=:service_id AND time_slot=:timeslot_id;");

			$query -> execute(array(
				"service_id" => $service_id,
				"timeslot_id" => $timeslot_id
			));
		}
	}

/**
*\brief supprime les emprunts liés à la plage horaire dont l'id du service et l'id du timeslot
*tous deux passés en paramètre
*/
function suppressionBorrowService($service_id, $timeslot_id)
	{
		$dbh = dbConnect();

		if($dbh != null)
		{
			$query = $dbh -> prepare("DELETE FROM borrow_service WHERE service=:service_id AND time_slot=:timeslot_id;");

			$query -> execute(array(
				"service_id" => $service_id,
				"timeslot_id" => $timeslot_id
			));
		}
	}

/**
*\brief supprime une plage horaire ainsi que les emprunts liés à celle-ci en se servant des fonctions précédentes
*\return une phrase indiquant si la suppression a été effectuée ou non
*/
function suppression()
	{

		$result = '';
		$email = null;

		$service_id = null;

		$dbh = dbConnect();

		if(isset($_SESSION['email']))
		{
			if($dbh != null)
			{
				$is_offered_id = $_POST['timeslot_suppression_id'];

				$list = explode("/",$is_offered_id);

				$timeslot_id = (int)$list[0];
				$service_id = (int)$list[1];

				//on supprime tous les horaires liés à ce service
				suppressionTimeslot($service_id, $timeslot_id);

				//on supprime tous les emprunts concernant ce service
				suppressionBorrowService($service_id, $timeslot_id);

				$result .= "Votre suppression d'horaire a bien été prise en compte !";
			}
			else
			{
				$result .= "Problème lors de la connection à la base de donnée, la suppression n'a pas pu être effectuée.";
			}
		}
		else
		{
			$result .= "Vous n'êtes pas connecté !";
		}

		return $result;
	}
?>