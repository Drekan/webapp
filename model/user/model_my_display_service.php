<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);

/**
*\brief Affichage des services proposés par l'utilisateur connecté
*\return String, html correspondant aux services de l'utilisateur
*
*
*
*/
function displayMyService()
	{
		$display = null;
		$email = null;

		if(isset($_SESSION['email']))
		{
			$email = $_SESSION['email'];
		}

		try
		{
			require('../secret.php');
			$dbh = new PDO('mysql:host=localhost; dbname=webapp; charset=UTF8', $user, $pass);

			$query = $dbh -> prepare("SELECT id,title, description, price
						FROM service
						WHERE email_member=:email
						ORDER BY id DESC");
			$query -> execute(array(
				"email" => $email
			));

			while($row = $query->fetch())
			{
				$display .= '<form method="post" action="manage_service_controller.php" id="display_my_service_form" class="display_service">';

				$display .= '<article >';
				$display .= '<p>'.$row['title'].'</p>';
				$display .= '<div class="description">'.$row['description'].'</div><br/>';
				$display .= '<p class="price">'.$row['price'].'€/heure</p>';
				$display .= '<div class="submit">';
				$display .= '<button type="submit" value="'.$row['id'].'" name="change_id">Modifier</button>';
				$display .= '</div>';
				$display .= '<div class="submit">';
				$display .= '<button type="submit" value="'.$row['id'].'" name="suppression_id">Supprimer</button>';
				$display .= '</div>';
				$display .= '<br/>';
				$display .= '</article>';

				$display .= '</form>';
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage()."<br/>\n";
			//die("Connexion impossible !");
		}

		return $display;
	}
?>