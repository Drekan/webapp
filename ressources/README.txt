.
├── controller 
├── index.php
├── model
├── public_html
│   ├── css
│   └── images
├── ressources
└── view
    ├── admins
    └── users

*** Répertoires et leur utilité ***
***********************************

-controller  ->    Contient tous les controllers qui font le lien entre
		les vues et les modèles. Il fait en sorte de toujours 
		afficher une page cohérente avec celui qui la demande.

-model       ->    Contient les modèles, CAD les fonctions qui gèrent 
		les données. Ne contient aucun affichage ! Que du PHP !

-public_html ->    Toute la partie statique du site est stockée ici, en
		l'occurence les feuilles de style CSS et les images.

-view        ->    Tous les fichiers qui s'occupent de l'affichage du
		site sont stockés ici. Ils récupèrent si besoin le résultat
		d'un modèle dans une variable PHP

-ressources  ->    Répertoire contenant tous les documents visants à faciliter
		la phase de dev. Il contient notamment el famoso README.txt,
		ce document très complet forgé par les dieux eux-mêmes. La légende
		raconte que Mark Zuckerberg utilisa ce petit fichier pour créer la
		première version de Facebook. Rien que ça.


*** Comment accéder à la base de donnée mysql en ligne de commande ***
**********************************************************************

/opt/lampp/bin/mysql -u root   (installation xampp requise)
ATTENTION , le SGBD doit être lancé depuis le répertoire scripts_sql (/--/webapp/ressources/scripts_sql/)


*** Lancer un script sql, une fois que le sgbd est lancé en ligne de commande ***
*********************************************************************************

source mon_script.sql;

(source load_database.sql;  --->  charge automatiquement tout les scripts)

ATTENTION , le SGBD doit être lancé depuis le répertoire scripts_sql (/--/webapp/ressources/scripts_sql/
)
