CREATE TABLE IF NOT EXISTS member(
	email VARCHAR(254),
	password VARCHAR(256),
	registration_date DATE,
	last_name VARCHAR(32),
	first_name VARCHAR(32),
	bs_offered INTEGER(10) default 0,
	bs_used INTEGER(10) default 0,
	money_earned INTEGER(10) default 0,
	money_spent INTEGER(10) default 0,
	PRIMARY KEY (email)
);

CREATE TABLE IF NOT EXISTS good(
	id INTEGER(21) AUTO_INCREMENT,
	title VARCHAR(30),
	description VARCHAR(180),
	price INTEGER,
	image_link VARCHAR(2083),
	email_member VARCHAR(256),
	PRIMARY KEY (id),
	FOREIGN KEY (email_member) REFERENCES member (email)
);

CREATE TABLE IF NOT EXISTS service(
	id INTEGER(21) AUTO_INCREMENT,
	title VARCHAR(30),
	description VARCHAR(180),
	price INTEGER,
	email_member VARCHAR(256),
	PRIMARY KEY(id),
	FOREIGN KEY (email_member) REFERENCES member (email)
);

CREATE TABLE IF NOT EXISTS time_slot(
	id INTEGER(21) AUTO_INCREMENT,
	start_time VARCHAR(10),
	end_time VARCHAR(10),
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS is_offered(
	time_slot INTEGER(21),
	service INTEGER(21),
	day VARCHAR(15),
	PRIMARY KEY(time_slot,service,day),
	FOREIGN KEY (time_slot) REFERENCES time_slot (id),
	FOREIGN KEY (service) REFERENCES service (id)
);

CREATE TABLE IF NOT EXISTS borrow_good(
	good INTEGER(21),
	email_member VARCHAR(256),
	day VARCHAR(15),
	PRIMARY KEY (good,email_member,day),
	FOREIGN KEY (good) REFERENCES good (id),
	FOREIGN KEY (email_member) REFERENCES member (email)
);

CREATE TABLE IF NOT EXISTS borrow_service(
	service INTEGER(21),
	time_slot INTEGER(21),
	email_member VARCHAR(256),
	day VARCHAR(15),
	PRIMARY KEY (service,time_slot,email_member,day),
	FOREIGN KEY (service) REFERENCES service (id),
	FOREIGN KEY (time_slot) REFERENCES time_slot (id),
	FOREIGN KEY (email_member) REFERENCES member (email)
);

CREATE TABLE IF NOT EXISTS admin(
	email VARCHAR(256),
	PRIMARY KEY(email),
	FOREIGN KEY (email) REFERENCES member(email)
);

CREATE TABLE IF NOT EXISTS notification(
	id INTEGER(21) AUTO_INCREMENT,
	creator VARCHAR(256),
	receiver VARCHAR(256),
	message VARCHAR(256),
	notif_type VARCHAR(256),
	date_borrow VARCHAR(10),
	PRIMARY KEY (id),
	FOREIGN KEY (creator) REFERENCES member (email),
	FOREIGN KEY (receiver) REFERENCES member (email)
);

CREATE TABLE IF NOT EXISTS suspended(
	email VARCHAR(256),
	suspention_date VARCHAR(21),
	suspention_duration VARCHAR(21),
	PRIMARY KEY (email),
	FOREIGN KEY (email) REFERENCES member (email)
);

source /opt/lampp/htdocs/webapp/ressources/scripts_sql/remove_warning.sql;
source /opt/lampp/htdocs/webapp/ressources/scripts_sql/procedure_notif.sql;
source /opt/lampp/htdocs/webapp/ressources/scripts_sql/set_trigger.sql;
source /opt/lampp/htdocs/webapp/ressources/scripts_sql/trigger_admin.sql;
