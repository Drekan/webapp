--Les timestamps ne sont pas exacts car normalement c'est dans une fonction php que nous le calculons
--Je considère que nous sommes un dimanche, pour emprunter un lundi il faut donc prendre le timestamp d'aujourd'hui et ajouter 86400 secondes
--Les prix ici ne tiennent pas compte de la durée d'un timeslot, le site le calcule automatique
INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (1,1,'trieuvan@yahoo.com',UNIX_TIMESTAMP(now())+(86400));
UPDATE member SET money_spent=money_spent+(select price from service where id=1) where email='trieuvan@yahoo.com';
UPDATE member SET bs_used=bs_used+1 where email='trieuvan@yahoo.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=1) where email='bdthomas@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='bdthomas@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (1,33,'edmeeMasson@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=1) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='edmeeMasson@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=1) where email='bdthomas@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='bdthomas@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (3,9,'edmeeMasson@gmail.com',UNIX_TIMESTAMP(now())(+86400*7));
UPDATE member SET money_spent=money_spent+(select price from service where id=3) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='edmeeMasson@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=3) where email='bdthomas@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='bdthomas@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (5,4,'crobles@msn.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=5) where email='crobles@msn.com';
UPDATE member SET bs_used=bs_used+1 where email='crobles@msn.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=5) where email='stern@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='stern@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (7,7,'edmeeMasson@gmail.com',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=7) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='edmeeMasson@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=7) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (7,8,'claudeDeniger@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=7) where email='claudeDeniger@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='claudeDeniger@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=7) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (8,20,'fallorn@mac.com',UNIX_TIMESTAMP(now())+(86400*10));
UPDATE member SET money_spent=money_spent+(select price from service where id=8) where email='fallorn@mac.com';
UPDATE member SET bs_used=bs_used+1 where email='fallorn@mac.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=8) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (8,20,'crobles@msn.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=8) where email='crobles@msn.com';
UPDATE member SET bs_used=bs_used+1 where email='crobles@msn.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=8) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (9,25,'trieuvan@yahoo.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=9) where email='trieuvan@yahoo.com';
UPDATE member SET bs_used=bs_used+1 where email='trieuvan@yahoo.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=9) where email='mkearl@outlook.com';
UPDATE member SET bs_offered=bs_offered+1 where email='mkearl@outlook.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (10,31,'christianFoucault@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=10) where email='christianFoucault@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='christianFoucault@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=10) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (10,32,'claudeDeniger@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=10) where email='claudeDeniger@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='claudeDeniger@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=10) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (11,24,'christianFoucault@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=11) where email='christianFoucault@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='christianFoucault@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=11) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (13,48,'nevillePlouffe@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=13) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='nevillePlouffe@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=13) where email='scovilleChartre@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='scovilleChartre@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (13,48,'patienceRivard@gmail.com',UNIX_TIMESTAMP(now())+(86400*10));
UPDATE member SET money_spent=money_spent+(select price from service where id=13) where email='patienceRivard@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='patienceRivard@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=13) where email='scovilleChartre@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='scovilleChartre@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (14,13,'nevillePlouffe@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=14) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='nevillePlouffe@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=14) where email='leakin@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='leakin@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (15,46,'nevillePlouffe@gmail.com',UNIX_TIMESTAMP(now())+(86400*10));
UPDATE member SET money_spent=money_spent+(select price from service where id=15) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='nevillePlouffe@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=15) where email='paulCamus@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='paulCamus@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (17,22,'stern@yahoo.com',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=17) where email='stern@yahoo.com';
UPDATE member SET bs_used=bs_used+1 where email='stern@yahoo.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=17) where email='paulCamus@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='paulCamus@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (20,15,'patienceRivard@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=20) where email='patienceRivard@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='patienceRivard@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=20) where email='paulCamus@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='paulCamus@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (20,16,'stern@yahoo.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=20) where email='stern@yahoo.com';
UPDATE member SET bs_used=bs_used+1 where email='stern@yahoo.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=20) where email='paulCamus@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='paulCamus@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (20,21,'trieuvan@yahoo.com',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=20) where email='trieuvan@yahoo.com';
UPDATE member SET bs_used=bs_used+1 where email='trieuvan@yahoo.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=20) where email='paulCamus@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='paulCamus@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (21,24,'paulCamus@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=21) where email='paulCamus@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='paulCamus@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=21) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (21,27,'seebs@outlook.com',UNIX_TIMESTAMP(now())+(86400*7));
UPDATE member SET money_spent=money_spent+(select price from service where id=21) where email='seebs@outlook.com';
UPDATE member SET bs_used=bs_used+1 where email='seebs@outlook.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=21) where email='trieuvan@yahoo.com';
UPDATE member SET bs_offered=bs_offered+1 where email='trieuvan@yahoo.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (23,31,'paulCamus@gmail.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=23) where email='paulCamus@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='paulCamus@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=23) where email='leakin@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='leakin@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (24,35,'grdschl@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=24) where email='grdschl@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='grdschl@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=24) where email='timotheeDesMeaux@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='timotheeDesMeaux@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (24,38,'paulCamus@gmail.com',UNIX_TIMESTAMP(now())+(86400*2));
UPDATE member SET money_spent=money_spent+(select price from service where id=24) where email='paulCamus@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='paulCamus@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=24) where email='timotheeDesMeaux@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='timotheeDesMeaux@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (26,46,'bdthomas@aol.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=26) where email='bdthomas@aol.com';
UPDATE member SET bs_used=bs_used+1 where email='bdthomas@aol.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=26) where email='timotheeDesMeaux@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='timotheeDesMeaux@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (28,2,'ferrauDouffet@gmail.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=28) where email='ferrauDouffet@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='ferrauDouffet@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=28) where email='konst@comcast.net';
UPDATE member SET bs_offered=bs_offered+1 where email='konst@comcast.net';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (30,41,'paulCamus@gmail.com',UNIX_TIMESTAMP(now())+(86400*11));
UPDATE member SET money_spent=money_spent+(select price from service where id=30) where email='paulCamus@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='paulCamus@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=30) where email='speeves@yahoo.ca';
UPDATE member SET bs_offered=bs_offered+1 where email='speeves@yahoo.ca';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (32,43,'grdschl@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*2));
UPDATE member SET money_spent=money_spent+(select price from service where id=32) where email='grdschl@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='grdschl@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=32) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (33,25,'leakin@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=33) where email='leakin@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='leakin@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=33) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (33,47,'ferrauDouffet@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=33) where email='ferrauDouffet@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='ferrauDouffet@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=33) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (37,52,'sydneyPiedalue@gmail.com',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=37) where email='sydneyPiedalue@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='sydneyPiedalue@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=37) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (38,4,'dkeeler@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=38) where email='dkeeler@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='dkeeler@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=38) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (38,60,'epoffoxur-8595@yopmail.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=38) where email='epoffoxur-8595@yopmail.com';
UPDATE member SET bs_used=bs_used+1 where email='epoffoxur-8595@yopmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=38) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (38,55,'julesBonami@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=38) where email='julesBonami@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='julesBonami@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=38) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (39,8,'dkeeler@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*2));
UPDATE member SET money_spent=money_spent+(select price from service where id=39) where email='dkeeler@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='dkeeler@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=39) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (39,45,'marlonCormier@gmail.com',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=39) where email='marlonCormier@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='marlonCormier@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=39) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (40,50,'capucineGaillard@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=40) where email='capucineGaillard@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='capucineGaillard@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=40) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (40,57,'marlonCormier@gmail.com',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=40) where email='marlonCormier@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='marlonCormier@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=40) where email='edmeeMasson@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='edmeeMasson@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (41,5,'huetteLaberge@gmail.com',UNIX_TIMESTAMP(now())+86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=41) where email='huetteLaberge@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='huetteLaberge@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=41) where email='anselRouleau@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='anselRouleau@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (41,25,'irving@me.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=41) where email='irving@me.com';
UPDATE member SET bs_used=bs_used+1 where email='irving@me.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=41 where email='anselRouleau@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='anselRouleau@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (44,19,'capucineGaillard@gmail.com',UNIX_TIMESTAMP(now())+(86400*3));
UPDATE member SET money_spent=money_spent+(select price from service where id=44) where email='capucineGaillard@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='capucineGaillard@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=44) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (45,11,'tarreau@aol.com',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=45) where email='tarreau@aol.com';
UPDATE member SET bs_used=bs_used+1 where email='tarreau@aol.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=45) where email='aurivilleSauve@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='aurivilleSauve@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (48,22,'yamla@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=48) where email='yamla@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='yamla@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=48) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (48,44,'yamla@yahoo.ca',UNIX_TIMESTAMP(now())+(86400*12));
UPDATE member SET money_spent=money_spent+(select price from service where id=48) where email='yamla@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='yamla@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=48) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (48,44,'sydneyPiedalue@gmail.com',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=48) where email='sydneyPiedalue@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='sydneyPiedalue@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=48) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (49,51,'matty@me.com',UNIX_TIMESTAMP(now())+(86400*8));
UPDATE member SET money_spent=money_spent+(select price from service where id=49) where email='matty@me.com';
UPDATE member SET bs_used=bs_used+1 where email='matty@me.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=49) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (49,51,'yamla@yahoo.ca',UNIX_TIMESTAMP(now())+(86400));
UPDATE member SET money_spent=money_spent+(select price from service where id=49) where email='yamla@yahoo.ca';
UPDATE member SET bs_used=bs_used+1 where email='yamla@yahoo.ca';
UPDATE member SET money_earned=money_earned+(select price from service where id=49) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (50,15,'irving@me.com',UNIX_TIMESTAMP(now())+(86400*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=50) where email='irving@me.com';
UPDATE member SET bs_used=bs_used+1 where email='irving@me.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=50) where email='sydneyPiedalue@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='sydneyPiedalue@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (52,41,'sydneyPiedalue@gmail.com',UNIX_TIMESTAMP(now())+(86400*11));
UPDATE member SET money_spent=money_spent+(select price from service where id=52) where email='sydneyPiedalue@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='sydneyPiedalue@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=52) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (52,41,'tarreau@aol.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=52) where email='tarreau@aol.com';
UPDATE member SET bs_used=bs_used+1 where email='tarreau@aol.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=52) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (55,32,'matty@me.com',UNIX_TIMESTAMP(now())86400);
UPDATE member SET money_spent=money_spent+(select price from service where id=55) where email='matty@me.com';
UPDATE member SET bs_used=bs_used+1 where email='matty@me.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=55) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (57,1,'claypool@sbcglobal.net',UNIX_TIMESTAMP(now())+(86400*5));
UPDATE member SET money_spent=money_spent+(select price from service where id=57) where email='claypool@sbcglobal.net';
UPDATE member SET bs_used=bs_used+1 where email='claypool@sbcglobal.net';
UPDATE member SET money_earned=money_earned+(select price from service where id=57) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (57,7,'anselRouleau@gmail.com',UNIX_TIMESTAMP(now())+(8600*6));
UPDATE member SET money_spent=money_spent+(select price from service where id=57) where email='anselRouleau@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='anselRouleau@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=57) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (57,7,'valentineTurgeon@gmail.com',UNIX_TIMESTAMP(now())+(86400*13));
UPDATE member SET money_spent=money_spent+(select price from service where id=57) where email='valentineTurgeon@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='valentineTurgeon@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=57) where email='terjesa@mac.com';
UPDATE member SET bs_offered=bs_offered+1 where email='terjesa@mac.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (58,42,'algernonGadbois@gmail.com',UNIX_TIMESTAMP(now())+(86400*4));
UPDATE member SET money_spent=money_spent+(select price from service where id=58) where email='algernonGadbois@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='algernonGadbois@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=58) where email='capucineGaillard@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='capucineGaillard@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (59,26,'valentineTurgeon@gmail.com',UNIX_TIMESTAMP(now()+(86400*6)));
UPDATE member SET money_spent=money_spent+(select price from service where id=59) where email='valentineTurgeon@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='valentineTurgeon@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=59) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (59,26,'claypool@sbcglobal.net',UNIX_TIMESTAMP(now())+(86400*13));
UPDATE member SET money_spent=money_spent+(select price from service where id=59) where email='claypool@sbcglobal.net';
UPDATE member SET bs_used=bs_used+1 where email='claypool@sbcglobal.net';
UPDATE member SET money_earned=money_earned+(select price from service where id=59) where email='tarreau@aol.com';
UPDATE member SET bs_offered=bs_offered+1 where email='tarreau@aol.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (60,1,'algernonGadbois@gmail.com',UNIX_TIMESTAMP(now())+(86400*7));
UPDATE member SET money_spent=money_spent+(select price from service where id=60) where email='algernonGadbois@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='algernonGadbois@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=60) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';

INSERT INTO borrow_service(service,time_slot,email_member,day) VALUES (60,1,'algernonGadbois@gmail.com',UNIX_TIMESTAMP(now())+(86400*14));
UPDATE member SET money_spent=money_spent+(select price from service where id=60) where email='algernonGadbois@gmail.com';
UPDATE member SET bs_used=bs_used+1 where email='algernonGadbois@gmail.com';
UPDATE member SET money_earned=money_earned+(select price from service where id=60) where email='nevillePlouffe@gmail.com';
UPDATE member SET bs_offered=bs_offered+1 where email='nevillePlouffe@gmail.com';
