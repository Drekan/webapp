DELIMITER |
CREATE TRIGGER check_admin AFTER DELETE
ON admin FOR EACH ROW
BEGIN
	DELETE FROM notification where creator=old.email;
END |
DELIMITER ;
