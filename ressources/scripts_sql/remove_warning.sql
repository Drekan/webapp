DELIMITER |
CREATE PROCEDURE removeWarning(IN userEmail VARCHAR(256))
BEGIN
delete from notification where receiver=userEmail AND notif_type='warning';
delete from notification where receiver=userEmail AND creator<>'NULL';
END |
DELIMITER ; 
