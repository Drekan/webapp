DELIMITER |
CREATE TRIGGER check_user_jackpot AFTER UPDATE
ON member FOR EACH ROW
BEGIN
	IF NEW.money_earned > 5 * NEW.money_spent
	AND NEW.money_earned > 50
	AND NEW.email NOT IN(SELECT email from admin)
	AND NEW.email NOT IN(SELECT receiver from notification where notif_type='warning')
		THEN
			set @user := NEW.email;
			CALL sendWarning(@user,'Pas assez d''argent dépensé !');
	END IF;
	
	IF NEW.money_spent > 5 * NEW.money_earned
	AND NEW.money_spent > 50
	AND NEW.email NOT IN(SELECT email from admin)
	AND NEW.email NOT IN(SELECT receiver from notification where notif_type='warning')
		THEN
			set @user := NEW.email;
			CALL sendWarning(@user,'Trop d''argent utilisé !');
	END IF;

	IF NEW.email IN(SELECT receiver from notification where notif_type='warning')
	AND NEW.money_spent <= 5 * NEW.money_earned
	AND NEW.money_earned <= 5 * NEW.money_spent
		THEN
			set @user := NEW.email;
			CALL removeWarning(@user);
	END IF;
END |
DELIMITER ;
