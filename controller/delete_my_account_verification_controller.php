<?php
session_start();
require('../model/user/model_delete_my_account_result.php');
require('../model/user/model_connection_info.php');


if(isset($_SESSION['email']))
{
	if(isset($_POST['supprimer']))
	{

		$result = suppressionAccount();
		$info_connection=getInfoConnection();
		require('../view/user/delete_my_account_result_view.php');
	}
	else if(isset($_POST['conserver']))
	{
		$info_connection=getInfoConnection();
		require('../model/user/model_my_profile.php');
		$jackpot_info=getJackpotInfo();
		require('../view/user/my_profile_view.php');


	}
}
