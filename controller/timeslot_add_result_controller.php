<?php
	session_start();
	require('../model/user/model_connection_info.php');

	$info_connection=getInfoConnection();

	if(isset($_SESSION['email']))
	{
		if(isset($_POST['add_id']))
		{
			require('../model/user/model_timeslot_add_result.php');

			$result = addTimeslot();

			require('../view/user/timeslot_add_result_view.php');
		}
	}
	