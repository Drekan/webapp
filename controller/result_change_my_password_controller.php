<?php 
	session_start();

	require('../model/user/model_change_my_password_result.php'); 
	require('../model/user/model_connection_info.php');

	$info_connection=getInfoConnection();
	$result = changePassword();

	if(isset($_SESSION['email']))
	{
		require('../view/user/change_my_password_result_view.php');
	}
?>
