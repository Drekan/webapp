<?php
	session_start();
	require('../model/user/model_connection_info.php');

	$info_connection=getInfoConnection();

	if(isset($_SESSION['email']))
	{	
		require('../model/user/model_my_profile.php');
		$jackpot_info=getJackpotInfo();
		require('../view/user/my_profile_view.php');
	
	}
	else
	{
		//ici la view de contenu non accessible aux non connectés
	}
