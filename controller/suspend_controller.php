<?php
	session_start();
	require('../model/user/model_connection_info.php');
	require('../model/user/model_suspend.php');

	$info_connection=getInfoConnection();
	$suspend=sendSuspension();

	require('../view/user/suspend_view.php');
