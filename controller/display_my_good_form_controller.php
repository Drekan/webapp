<?php
	session_start();
	require('../model/user/model_connection_info.php');
	require('../model/user/model_my_display_good.php');

	$info_connection = getInfoConnection();
	$display = displayMyGood();

	if(isset($_SESSION['email']))
	{
		require('../view/user/display_my_good_form_view.php');
	}
?>