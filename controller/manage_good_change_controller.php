<?php
	session_start();
	require('../model/user/model_result_manage_good_change.php');
	require('../model/user/model_connection_info.php');
	
	$change_result = getChangeResult();
	$info_connection = getInfoConnection();
	
	require('../view/user/result_manage_good_change_view.php');
?>