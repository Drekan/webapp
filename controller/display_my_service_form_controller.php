<?php
	session_start();
	require('../model/user/model_connection_info.php');
	require('../model/user/model_my_display_service.php');

	$info_connection = getInfoConnection();
	$display = displayMyService();

	if(isset($_SESSION['email']))
	{
		require('../view/user/display_my_service_form_view.php');
	}
?>