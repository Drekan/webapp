<?php
	session_start();
	require('../model/user/model_connection_info.php');
	require('../model/user/model_manage_timeslot_choice.php');

	$info_connection = getInfoConnection();
	$result = timeslotService();

	if(isset($_SESSION['email']))
	{
		require('../view/user/manage_timeslot_choice_view.php');
	}
	else
	{
		//nope, pas de connexion, pas de formulaire
	}
