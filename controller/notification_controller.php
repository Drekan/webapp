<?php

session_start();
require('../model/user/model_connection_info.php');
require('../model/user/model_notification.php');

$info_connection=getInfoConnection();
$notification_display=getNotificationDisplay();

require('../view/user/notification_view.php');
