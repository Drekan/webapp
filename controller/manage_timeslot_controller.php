<?php
	session_start();
	require('../model/user/model_connection_info.php');

	$info_connection=getInfoConnection();

	if(isset($_SESSION['email']))
	{
		if(isset($_POST['timeslot_change_id']))
		{
			require('../view/user/manage_timeslot_view.php');
		}
		else if(isset($_POST['timeslot_suppression_id']))
		{
			require('../model/user/model_timeslot_suppression.php');

			$result = suppression();

			require('../view/user/timeslot_suppression_view.php');
		}
	}
