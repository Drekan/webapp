<?php
	session_start();
	require('../model/user/model_connection_info.php');

	$info_connection = getInfoConnection();

	if(isset($_SESSION['email']))
	{
		if(isset($_POST['good_id']))
		{
			require('../model/user/model_manage_good.php');

			$display = displayManageGood();

			require('../view/user/manage_good_view.php');
		}
		else if(isset($_POST['suppression_good_id']))
		{
			require('../model/user/model_result_good_suppression.php');

			$display = SuppressionGood();

			require('../view/user/result_good_suppression_view.php');
		}
	}
?>