<?php
	session_start();
	require('../model/user/model_connection_info.php');
	require('../model/user/model_display_service.php');

	$info_connection = getInfoConnection();
	$display = displayService();

	require('../view/user/display_service_form_view.php');
?>