<?php
	session_start();
	require('../model/user/model_connection_info.php');

	$info_connection = getInfoConnection();

	if(isset($_SESSION['email']))
	{
		if(isset($_POST['change_id']))
		{
			require('../model/user/model_manage_service.php');

			$display = displayManageService();

			require('../view/user/manage_service_view.php');
		}
		else if(isset($_POST['suppression_id']))
		{
			require('../model/user/model_result_service_suppression.php');

			$display = suppressionService();

			require('../view/user/result_service_suppression_view.php');
		}
	}
?>
