<?php $title="Mes disponibilités";
	ob_start(); ?>

<?= $previous_timeslot; ?>

<form method="post" action="timeslot_form_controller.php" class="form_display_timeslot">
<div  class="display_timeslot">
<label for="day">Jour :
<select name="day">
	<option value="Lundi">Lundi</option>
	<option value="Mardi">Mardi</option>
	<option value="Mercredi">Mercredi</option>
	<option value="Jeudi">Jeudi</option>
	<option value="Vendredi">Vendredi</option>
	<option value="Samedi">Samedi</option>
	<option value="Dimanche">Dimanche</option>
</select>
</label>
</div>
<div  class="display_timeslot_1">
<label for="start_hour">Heure début : 
<select name="start_hour">

<?php for($i=0;$i<24;$i++){ ?>
	<option value="<?= $i ?>"><?= $i . 'h' ?></option>
<?php } ?>

</select>
</label>
 
<select name="start_minute">

<?php for($i=0;$i<12;$i++){ ?>
	<option value="<?= ($i==0?'00':$i*5) ?>"><?= ($i==0?'00':$i*5) ?></option>
<?php } ?>

</select>
</div>
<div  class="display_timeslot_2">
<label for="end_hour">Heure de fin : 
<select name="end_hour">

<?php for($i=0;$i<24;$i++){ ?>
	<option value="<?= $i ?>"><?= $i . 'h' ?></option>
<?php } ?>

</select>
</label>
 
<select name="end_minute">

<?php for($i=0;$i<12;$i++){ ?>
	<option value="<?= ($i==0?'00':$i*5) ?>"><?= ($i==0?'00':$i*5) ?></option>
<?php } ?>

</select>


<input type="submit" value="Je valide cet horaire"/>
</div>
</form>
<form action="service_result_controller.php" class="historique_timeslot">
<input type="submit" value="Poster mon service"/>
</form>
<?php 
	$content=ob_get_clean();
	require('template.php');
?>


	
