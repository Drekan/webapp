<?php $title = "Louer un service";
	
	ob_start(); ?>

<div class="general_information">
<h2>Proposer un service</h2>
<p>Les champs précédés d'un (*) sont obligatoires. </p>
</div>
<form method="post" action="timeslot_form_controller.php" id="good_form" class="manage_service">
	<label for="title">
		*Titre : <input type="text" name="title" required/>
	</label><br/>

	<label for="description">Description :
		<textarea name="description" maxlength="180" form="good_form"></textarea>
	</label><br/>
	
	<label for="price">
		*Prix à l'heure : <input type="number" name="price" required/>
	</label><br/>

	<input type="submit" value="Déposer l'annonce"/>
	
</form>



<?php 
	$content=ob_get_clean();
	require('template.php'); ?>
