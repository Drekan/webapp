<?php $title = "Louer un bien";
	
	ob_start(); ?>

<div class="general_information">
<h2>Proposer un bien</h2>
<p>Les champs précédés d'un (*) sont obligatoires. </p>
</div>
<form method="post" action="good_result_controller.php" id="good_form" class="manage_good">
	<label for="title">
		*Titre : <input type="text" name="title" required/>
	</label><br/>

	<label for="description">Description :
		<textarea name="description" maxlength="180" form="good_form"></textarea>
	</label><br/>
	
	<label for="image_link">
		Lien de l'image : <input type="text" name="image_link" maxlength="2083"/>
	</label><br/>
	
	<label for="price">
		*Prix du bien neuf : <input type="number" name="price" required/>
	</label><br/>

	<input type="submit" value="Déposer l'annonce"/>
	
</form>



<?php 
	$content=ob_get_clean();
	require('template.php'); ?>
