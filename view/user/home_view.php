<?php	
$title='Accueil';
ob_start(); ?>
<div class="homepage">
<div class="homepage_article1">
	<h1>Échangez, proposez, négociez</h1>
	<p><br>
	<b>Louez du matériel</b>, demandez de l'aide, proposez vos bien et service et arrondissez vos fins de mois. Près de chez vous et partout en France !
	Une personne sur deux garde chez elle des objets qui ne lui servent plus à rien et se laisse peu à peu envahir. Dommage de gaspiller ainsi des mètres carrés. Ne faites plus dormir vos objets et votre savoir-faire et <a href="registration_controller.php">inscrivez-vous</a> !
	</p>
</div>
<div class="homepage_article2">
	<h1>Le monde du circuit court s'offre à vous</h1>
	<p>
	<b>Un mode de distribution plus respectueux de l’environnement</b> : comparé à un mode de distribution classique, le transport des marchandises est diminué. Dans le cas d’une vente directe réalisée sur l’exploitation même, elle est quasi-nulle. Circuit court rime aussi avec moins de gaspillage : les critères de sélection de la grande distribution, basé sur un calibrage précis des produits, entraîne des pertes inutiles.<br>
<b>Ayez la main sur le prix</b> : vous êtes maître du prix de votre production. En ne passant plus par les intermédiaires, vous êtes libres de choisir le prix de vos produits.
</p>
</div>
</div>
<?php $content=ob_get_clean(); ?>

<?php require('template.php'); ?>
