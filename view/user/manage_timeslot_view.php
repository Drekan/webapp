<?php $title="Modification d'horaires";
	ob_start(); ?>

<form method="post" action="manage_timeslot_result_controller.php">
<label for="day">Jour :
<select name="day">
	<option value="Lundi">Lundi</option>
	<option value="Mardi">Mardi</option>
	<option value="Mercredi">Mercredi</option>
	<option value="Jeudi">Jeudi</option>
	<option value="Vendredi">Vendredi</option>
	<option value="Samedi">Samedi</option>
	<option value="Dimanche">Dimanche</option>
</select>
</label>

<label for="start_hour">Heure début : 
<select name="start_hour">

<?php for($i = 0; $i < 24; $i++){ ?>
	<option value="<?= $i ?>">
		<?php 
			if($i < 10)
			{
				echo '0'.$i.'h';
			}
			else
			{
				echo $i.'h';
			}
		?>
	</option>
<?php } ?>

</select>
</label>
 
<select name="start_minute">

<?php for($i = 0; $i < 12; $i++){ ?>
	<option value="<?= $i*5 ?>">
		<?php 
			if($i*5 < 10)
			{
				echo '0'.$i*5;
			}
			else
			{
				echo $i*5;
			}
		?>
	</option>
<?php } ?>

</select>

<label for="end_hour">Heure de fin : 
<select name="end_hour">

<?php for($i = 0;$i < 24; $i++){ ?>
	<option value="<?= $i ?>">
		<?php 
			if($i < 10)
			{
				echo '0'.$i.'h';
			}
			else
			{
				echo $i.'h';
			}
		?>
	</option>
<?php } ?>

</select>
</label>
 
<select name="end_minute">

<?php for($i = 0; $i < 12; $i++){ ?>
	<option value="<?= $i*5 ?>">
		<?php 
			if($i*5 < 10)
			{
				echo '0'.$i*5;
			}
			else
			{
				echo $i*5;
			}
		?>
	</option>
<?php } ?>

</select>


<button type="submit" value="
	<?php 
		$is_offered_id = null;

		if(isset($_POST['timeslot_change_id']))
		{
			$is_offered_id = $_POST['timeslot_change_id'];

		}

	echo $is_offered_id;
	?>" name="timeslot_id">Valider ces modifications</button>

<?php 
	$content=ob_get_clean();
	require('template.php');
?>
