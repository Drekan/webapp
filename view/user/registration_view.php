<?php $title = "Inscription"; ?>


<?php ob_start(); ?>
	<form method="post" action="result_registration_controller.php" class="registration">
		<label for="email">Email : <input type="email" name="email" required/></label><br/>
		<label for="last_name">Nom : <input type="text" name="last_name" required/></label><br/>
		<label for="first_name">Prénom : <input type="text" name="first_name" required/></label><br/>
		<label for="password">Mot de passe : <input type="password" name="password" required/></label><br/>
		<label for="password_confirmation">Confirmez votre mot de passe : <input type="password" name="password_confirmation" required/></label><br/>
		<input type="submit" value="Valider"/>
	</form>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
