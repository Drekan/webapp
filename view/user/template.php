<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../public_html/css/style.css">
		<title><?= $title ?></title>
    </head>        

    <header>
    	<nav>
			<ul  class="menu">
					<li><a  href="home_controller.php">Accueil</a>  </li>
					<li><a  href="display_choice_controller.php">Consulter les annonces</a>  </li>
        			<?= $info_connection ?>
			</ul>
		</nav>
    </header>
    
    <body>
        <?= $content ?>
    </body>

    <footer>
    	<!-- <?= $footer ?>	-->		<!-- Je l'ai mis au cas où on fasse un footer mais on n'est pas obligé de le garder -->
    </footer>
</html>
