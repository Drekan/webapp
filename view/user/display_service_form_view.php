<?php 
	$title = "Affichage des services";
	ob_start(); ?>

		<form method="post" action="display_service_form_controller.php" id="service_search_form" class="search">
			<input type="search" name="search_word" placeholder="Veuillez saisir un mot-clef" />
			<div id="s-circle"></div>
		</form>

		<?= $display?>

<?php 
	$content = ob_get_clean();
	require('template.php'); ?>
