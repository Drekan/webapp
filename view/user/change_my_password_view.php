<?php $title = "Changement de mot de passe"; ?>


<?php ob_start(); ?>
	<form method="post" action="result_change_my_password_controller.php" class="change_password" id="contact">
		<label for="password">Nouveau mot de passe : <input type="password" name="new_password" required/></label>
		<label for="password_confirmation">Confirmez votre nouveau mot de passe : <input type="password" name="new_password_confirmation" required/></label>
		<input type="submit" value="Valider"/>
	</form>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
